<?php

/**
 * default.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Lovelyshow_Controller extends TinyMVC_Controller
{
  function index()
  {
    $this->view->display('index_view');
  }
  
  function about()
  {
  	$this->view->display('index_about');
  }
  
  function terms()
  {
  	$this->view->display('index_rules');
  }
  
  function schedule()
  {
  	$this->view->display('index_schedule');
  }
  
  function news()
  {
  	$this->view->display('index_news');
  }
  
  function newsdetail()
  {
  	$this->view->display('index_detail_news');
  }
}

?>
