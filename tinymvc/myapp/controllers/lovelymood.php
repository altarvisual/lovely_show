<?php

/**
 * default.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Lovelymood_Controller extends TinyMVC_Controller
{
  function index()
  {
    $this->view->display('index_lovelymood');
  }
}

?>
