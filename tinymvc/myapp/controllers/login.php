<?php

/**
 * default.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Login_Controller extends TinyMVC_Controller
{
  function index() {
  	session_start();
  	$_SESSION['video_id'] = $_GET['v'];
    $this->view->display('index_login');
  }
  
  function like() {
  	session_start();
  	$video_id = substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],__FUNCTION__)+strlen(__FUNCTION__)+1);
  	$_SESSION['video_id'] = $video_id;
    $this->view->display('index_login');
  }
  
  function dologin() {
  	$email = $_POST["email"];
  	$password = md5($_POST["password"]);
  	$this->load->model('User_Model','user');
  	$success = $this->user->auth_login($email,$password);
  	
  	if($success == true) {
  		session_start();
  		$_SESSION['username'] = $email;
//   		$this->view->display('index_view');
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/home');
  	} else {
  		header('Location: http://'.$_SERVER['HTTP_HOST'].'/login');
  	}
  }
  
  function dologin_fb() {
  	session_start();
  	$video_id = $_SESSION['video_id'];
  	$this->load->library('facebook','facebook');
  	$this->facebook->login($video_id);
  	 
  	if($success == true) {
  		session_start();
//   		$_SESSION['username'] = $email;
  		//   		$this->view->display('index_view');
  		header('Location: http://'.$_SERVER['HTTP_HOST'].'/home');
  	} else {
  		header('Location: http://'.$_SERVER['HTTP_HOST'].'/login');
  	}
  }
  
  function oauth_fb() {
  	
  }
  
  function dologin_twitter() {
  	session_start();
  	$video_id = $_SESSION['video_id'];
  	$this->load->library('twitter','twitter');
  	$this->twitter->login($video_id);
  
  	if($success == true) {
//   		session_start();
  		//   		$_SESSION['username'] = $email;
  		//   		$this->view->display('index_view');
  		header('Location: http://'.$_SERVER['HTTP_HOST'].'/home');
  	} else {
  		header('Location: http://'.$_SERVER['HTTP_HOST'].'/login');
  	}
  }
  
  function oauth_twitter() {
  	$this->load->library('twitter','twitter');
  	$this->twitter->oauth();
  }
}

?>
