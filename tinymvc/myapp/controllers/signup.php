<?php

/**
 * default.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Signup_Controller extends TinyMVC_Controller
{
  function index()
  {
	$this->view->display('index_signup');
  }
  
  function captcha() {
  	session_start();
  	$text = rand(10000,99999);
  	$_SESSION["vercode"] = $text;
  	$height = 25;
  	$width = 65;
  	 
  	$image_p = imagecreate($width, $height);
  	$black = imagecolorallocate($image_p, 0, 0, 0);
  	$white = imagecolorallocate($image_p, 255, 255, 255);
  	$font_size = 14;
  	imagestring($image_p, $font_size, 5, 5, $text, $white);
  	imagejpeg($image_p, null, 80);
  	header('Content-type:image/png');
  }
  
  function get_mysql_date_format($date) {
  	$token = strtok($date, "/");
  	$dateResult=array();
  	while ($token !== false) {
  		array_push($dateResult, $token);
  		$token = strtok("/");
  	}
  	$birthdate = $dateResult[2].'-'.$dateResult[0].'-'.$dateResult[1];
  	return $birthdate;
  }
  
  function doregister() {
	$this->load->model('User_Model','user');
  	$birthdate = $this->get_mysql_date_format($_POST ["birthdate"]);
	$user = $this->user->get_userid_by_email($_POST["email"]);
	if(!isset($user['user_id'])){
		$user = array(
				"email" => $_POST["email"],
				"password" => md5 ( $_POST ["password"] ),
				"name" => $_POST ["name"],
				"gender" => $_POST ["gender"],
				"birthplace" =>  $_POST ["birthplace"],
				"birthdate" => $birthdate,
				"telp" => $_POST ["telp"],
		);
		
		$this->user->reg_user($user);
		$this->view->display('index_view');
	} else {
		$this->view->display('index_signup');
	}
	
  }
  
  function doregister_google() {
  	$this->load->model('User_Model','user');
  	$user = $this->user->get_userid_by_email($_POST['email']);
  	if(!isset($user['user_id'])){
  		$user = array(
  				"email" => $_POST['email'],
  				"address" => $_POST['address'],
  				"telp" => $_POST['telp'],
  				"name" => $_POST['name']
  		);
  			
  		$this->user->reg_user_google($user);
  	
  	}
  }
}

?>
