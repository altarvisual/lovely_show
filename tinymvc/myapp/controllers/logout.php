<?php

/**
 * default.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Logout_Controller extends TinyMVC_Controller
{
  function index()
  {
	session_start(); //to ensure you are using same session
  	session_unset();
  	if(isset($_SESSION['username']))
  		unset($_SESSION['username']);
    header('Location: http://'.$_SERVER['HTTP_HOST'].'/home');
  }
  
}

?>
