<?php

/**
 * default.php
 *
 * default application controller
 *
 * @package		TinyMVC
 * @author		Monte Ohrt
 */

class Dancecontest_Controller extends TinyMVC_Controller
{
  function index()
  {
    $this->view->display('index_view');
  }
  
  function download()
  {
//   	$file = 'http://'.$_SERVER['HTTP_HOST'].'/music/JKT48-PUTERI-60-(FULL-SONG).mp3';
//   	header ("Content-type: octet/stream");
//   	header ("Content-disposition: attachment; filename=".$file.";");
//   	header("Content-Length: ".filesize($file));
//   	readfile($file);
//   	exit;
  	header ("Content-type: octet/stream");
  	header ("Content-Disposition: attachment");
  	header('Location: http://'.$_SERVER['HTTP_HOST'].'/music/Mustika_Puteri-JKT48.zip'); 
//   	readfile('JKT48-PUTERI-60-(FULL-SONG).mp3');
  }
  
  function howtojoin()
  {
  	$this->view->display('index_howjoin');
  }
  
  function rules()
  {
  	$this->view->display('index_rules');
  }
  
  function upload()
  {
	 session_start();
	 if(!isset($_SESSION['username'])){
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/login');
	 } else {
		$this->load->model('Video_Model','video');$this->load->model('User_Model','user');
		$this->load->model('User_Model','user');
		$user_id = $this->user->get_userid_by_email($_SESSION['username']);
		$has_been_uploaded = $this->video->has_been_uploaded($user_id['user_id']);
		if($has_been_uploaded == true) {
			$message = "Kamu hanya bisa sekali melakukan upload video";
			$this->view->assign('message',$message);
			$this->view->display('index_confirm');
		} else {
			$this->view->display('index_upload');
		}
		
	 }
  }
  
  function doupload()
  {
  	session_start();
  	$this->load->model('User_Model','user');
	$user_id = $this->user->get_userid_by_email($_POST["email"]);
  	$video = array(
  			"title" => $_POST["title"],
  			"desc" => $_POST ["desc"],
  			"group" => $_POST ["group"],
  			"link" => $_POST ["link"],
  			"user_id" => $user_id['user_id'],
  			"city" => $_POST ["city"]
  	);
  	
//   	$target_file = basename($_FILES["file"]["name"]);
//   	$this->load->library('youtube','youtube');
//   	$this->view->assign('output', $this->youtube->upload($target_file));
//   	$this->view->display('index_upload');
  	
  	$this->load->model('Video_Model','video');
  	$user_id = $this->video->upload_video($video);
  	header('Location: http://'.$_SERVER['HTTP_HOST'].'/home');
  }
  
  function dolike_fb() {
  	session_start();
  	$video_id = $_SESSION['video_id'];
  	$this->load->model('User_Model','user');
  	$user = $this->user->get_userid_by_email($_SESSION['email']);
  	if(!isset($user['user_id'])){
  		$user = array(
  				"email" => $_SESSION['email'],
  				"name" => $_SESSION['fullname']
  		);
  			
  		$this->user->reg_user_fb($user);
  		
  	} 
  	
  	$this->load->model('Video_Model','video');
  	$user_id = $this->user->get_userid_by_email($_SESSION['email']);
  	$like = array(
  			"video_id" => $video_id,
  			"user_id" => $user_id['user_id']
  	);
  	$has_been_liked = $this->video->has_been_liked($like);
  	if($has_been_liked == true) {
  		$message = "Kamu hanya bisa sekali melakukan voting terhadap video yang sama";
  		$this->view->assign('message',$message);
  		$this->view->display('index_confirm');
  	} else {
  		$this->video->like_video($like);
  		$message = "Terima kasih telah berpartisipasi melakukan vote video";
  		$this->view->assign('message',$message);
  		$this->view->display('index_confirm');
  	}
  }
  
  function dolike()
  {
  	session_start();
	if(!isset($_SESSION['username'])) {
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/login');
	} else {
		$this->load->model('User_Model','user');
		$this->load->model('Video_Model','video');
		$user_id = $this->user->get_userid_by_email($_SESSION['username']);
		$video_id = $_GET['id'];
		$like = array(
				"video_id" => $video_id = $video_id,
				"user_id" => $user_id['user_id']
		);
		$has_been_liked = $this->video->has_been_liked($like);
		if($has_been_liked == true) {
			$message = "Kamu hanya bisa sekali melakukan voting terhadap video yang sama";
			$this->view->assign('message',$message);
			$this->view->display('index_confirm');
		} else {
			$this->video->like_video($like);
			$message = "Terima kasih telah berpartisipasi melakukan vote video";
			$this->view->assign('message',$message);
			$this->view->display('index_confirm');
		}
		
		//header('Location: http://'.$_SERVER['HTTP_HOST'].'/dancecontest/video?v='.$video_id);
	}
  	
  }
  
  function gallery()
  {
  	$param = substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],__FUNCTION__)+strlen(__FUNCTION__)+1);
  	$sortby = "";
  	$name = "";
  	$city = "";
  	$page = 1;
  	$start = 0;
  	$end = 9;
  	$token = strtok($param, "/");
  	$i=1;
  	while ($token !== false)
    {
    	if($i==1) $sortby = $token;
    	else if($i==2) $page = $token;
    	else if($i==3) $city = $token;
    	else if($i==4) $name = $token; 
	    $token = strtok("/");
	    $i++;
    }
    
    if($city == "all") $city = "";
    
    if($page > 1) {
    	$multiply = $page * 9;
    	$start = $multiply-9;
//     	$end = $multiply;
    }
    
    
  	$this->load->model('Video_Model','video');
  	$count = $this->video->count_video($name,$city);
  	
  	$total_page = $count['total'] / 9;
//   	echo $count['total'].'  ';
//   	echo ceil($total_page);
// echo $start.' '.$end;
  	
  	if($sortby == ""){
  		$videos = $this->video->get_all_videos($name,$city,$start,$end);
  	} else if($sortby == "latest"){
  		$videos = $this->video->get_all_videos($name,$city,$start,$end);
  	} else if($sortby == "viewed"){
  		$videos = $this->video->get_all_videos_sort_view($name,$city,$start,$end);
  	} else if($sortby == "liked"){
  		$videos = $this->video->get_all_videos_sort_liked($name,$city,$start,$end);
  	} else {
  		$videos = $this->video->get_all_videos($name,$city,$start,$end);
  	}
  	
  	$url = 'http://'.$_SERVER['HTTP_HOST'].'/dancecontest/gallery/'.$sortby.'/';
  	$this->view->assign('url',$url);
  	$this->view->assign('city',$city);
  	$this->view->assign('name',$name);
  	$this->view->assign('total_page',ceil($total_page));
  	$this->view->assign('videos',$videos);
  	$this->view->display('index_gallery');
  }
  
  function video()
  {
  	$video_id = substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],__FUNCTION__)+strlen(__FUNCTION__)+1);
//   	echo strpos($_SERVER['REQUEST_URI'],__FUNCTION__);
//    	$a = $_SERVER['REQUEST_URI'];
//    	echo __FUNCTION__."<br>";
//    	echo $a;
//   	$video_id = $_GET['v'];
  	$this->load->model('Video_Model','video');
  	$video = $this->video->get_video_by_videoid($video_id);
  	$this->view->assign('video',$video);
	$this->video->add_hit_video($video_id,$video['hits']);
  	$this->view->display('index_video');
  }
  
  function winners()
  {
  	$this->view->display('index_finalist');
  }
}

?>
