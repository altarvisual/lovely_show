<?php

class Video_Model extends TinyMVC_Model
{
	function upload_video($video) {
		return $this->db->insert('mp_video',array('title'=>$video['title'],
												 'desc'=>$video['desc'],
												 'group'=>$video['group'],
												 'city'=>$video['city'],
												 'youtube_link'=>$video['link'],
												 'lst_upd'=>date('Y-m-d H:i:s'),
												 'user_id'=>$video['user_id']
												 
		));
	}
	
	function add_hit_video($video_id,$hits) {
		$this->db->where('video_id',$video_id);
		return $this->db->update('mp_video',array('hits'=>$hits+1));
	}
	
	function update_link($link) {
		$this->db->where('link',$link);
		return $this->db->update('mp_video',array('hits'=>$hits+1));
	}
	
	function get_total_likes_by_video($like) {
		return $this->db->query_one('select sum(user_id) from mp_liked_video where video_id=?',array($video_id));
	}
	
	function like_video($like) {
		return $this->db->insert('mp_liked_video',array('video_id'=>$like['video_id'],
														'user_id'=>$like['user_id'],
														'lst_upd'=>date('Y-m-d H:i:s')
					
		));
	}
	
	function has_been_liked($like) {
		$this->db->query ( 'select * from mp_liked_video where video_id=? and user_id=?'
						,array($like['video_id'], $like['user_id'] ));
		if ($row = $this->db->next())
			return true;
		else 
			return false;
	}
	
	function has_been_uploaded($user_id) {
		$this->db->query ( 'select * from mp_video where user_id=?' ,array($user_id));
		if ($row = $this->db->next())
			return true;
		else 
			return false;
	}
	
	function get_video_by_videoid($video_id) {
		return $this->db->query_one('select * from mp_video where video_id=?',array($video_id));
	}
	
	function count_video($title,$city) {
		return $this->db->query_one ( 'select count(1) total from  mp_video v
								where title like ?
								and city like ?',array('%'.$title.'%','%'.$city.'%'));
	}
	
	function get_all_videos($title,$city,$start,$end) {
		$results = array();
		$this->db->query ( 'select v.*,DATE_FORMAT(v.lst_upd,\'%d %M\') upload_date,lv.video_id video_like,CASE WHEN lv.video_id is NULL THEN 0
							ELSE count(1)
							END AS total_like from  mp_video v
							LEFT JOIN mp_liked_video lv
							ON lv.video_id=v.video_id
							where title like ?
							and city like ?
							group by v.video_id 
							order by v.lst_upd desc 
							limit '.$start.','.$end ,array('%'.$title.'%','%'.$city.'%'));
		while ($row = $this->db->next())
			$results [] = $row;
		return $results;
	}
	
	function get_all_videos_sort_view($title,$city,$start,$end) {
		$results = array();
		$this->db->query ( 'select v.*,DATE_FORMAT(v.lst_upd,\'%d %M\') upload_date,lv.video_id video_like,CASE WHEN lv.video_id is NULL THEN 0
							ELSE count(1)
							END AS total_like from  mp_video v
							LEFT JOIN mp_liked_video lv
							ON lv.video_id=v.video_id
							where title like ?
							and city like ?
							group by v.video_id
							order by hits desc 
							limit '.$start.','.$end,array('%'.$title.'%','%'.$city.'%') );
		while ($row = $this->db->next())
			$results [] = $row;
		return $results;
	}
	
	function get_all_videos_sort_liked($title,$city,$start,$end) {
		$results = array();
		$this->db->query ( 'select v.*,DATE_FORMAT(v.lst_upd,\'%d %M\') upload_date,lv.video_id video_like,
							CASE WHEN lv.video_id is NULL THEN 0
							ELSE count(1)
							END AS total_like from  mp_video v
							LEFT JOIN mp_liked_video lv
							ON lv.video_id=v.video_id
							where title like ?
							and city like ?
							group by v.video_id
							order by total_like desc
							limit '.$start.','.$end ,array('%'.$title.'%','%'.$city.'%') );
		while ($row = $this->db->next())
			$results [] = $row;
		return $results;
	}
}

?>