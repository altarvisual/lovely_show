<?php

class User_Model extends TinyMVC_Model {

	function auth_login($email,$password) {
	  $this->db->query('select 1 from mp_user where email=? and password=?',array($email,$password));
	  if ($row = $this->db->next()) {
	  	return true;
	  } else {
	  	return false;
	  }
	  	
	}
	
	function get_userid_by_email($email) {
		return $this->db->query_one('select user_id from mp_user where email=?',array($email));
	}
	
	function reg_user($user) {
		return $this->db->insert('mp_user',array('email'=>$user['email'],
												 'password'=>$user['password'],
												 'name'=>$user['name'],
												 'gender'=>$user['gender'],
												 'birth_date'=>$user['birthdate'],
												 'birth_place'=>$user['birthplace'],
												 'telp'=>$user['telp'],
												 'lst_upd'=>date('Y-m-d H:i:s')
		));
	}
	
	function reg_user_fb($user) {
		return $this->db->insert('mp_user',array('email'=>$user['email'],
				'name'=>$user['name'],
				'lst_upd'=>date('Y-m-d H:i:s')
		));
	}
	
	function reg_user_google($user) {
		return $this->db->insert('mp_user',array('email'=>$user['email'],
				'name'=>$user['name'],
				'address'=>$user['address'],
				'telp'=>$user['telp'],
				'lst_upd'=>date('Y-m-d H:i:s')
		));
	}

}
?>