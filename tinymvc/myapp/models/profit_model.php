<?php

class Profit_Model extends TinyMVC_Model
{
  function calc_sell_minus_buy($brokerid)
  {
    return $this->db->query_one('select total_sell-total_buy calc from 
					(
						select a.total total_buy, b.total total_sell from 
						(
							(
								select sum(qty*price*100) amt, sum(qty*price*100)*buy_fee fee, (sum(qty*price*100))+(sum(qty*price*100)*buy_fee) total from trade t, broker b
								where t.broker_id = b.broker_id
								and t.broker_id=?
								and buysell=?
							) a
							join
							(
								select sum(qty*price*100) amt, sum(qty*price*100)*sell_fee fee, (sum(qty*price*100))-(sum(qty*price*100)*sell_fee) total from trade t, broker b
								where t.broker_id = b.broker_id
								and t.broker_id=?
								and buysell=?
							) b
						)
					) abc',array($brokerid,'B',$brokerid,'S'));
  }
  
  function calc_sell_minus_buy_by_stock($brokerid)
  {
  	$results = array();
  	$this->db->query('select a.stock,a.total total_buy, b.total total_sell, b.total-a.total diff, (b.total-a.total)/a.total*100 perc from 
								(
									select stock,sum(qty*price*100) amt, sum(qty*price*100)*buy_fee fee, (sum(qty*price*100))+(sum(qty*price*100)*buy_fee) total from trade t, broker b
									where t.broker_id = b.broker_id
									and t.broker_id=?
									and buysell=?
	                                group by stock
								) a
								join
								(
									select stock,sum(qty*price*100) amt, sum(qty*price*100)*sell_fee fee, (sum(qty*price*100))-(sum(qty*price*100)*sell_fee) total from trade t, broker b
									where t.broker_id = b.broker_id
									and t.broker_id=?
									and buysell=?
	                                group by stock
								) b on a.stock=b.stock',array($brokerid,'B',$brokerid,'S'));
  	while($row = $this->db->next())
  		$results[] = $row;
  	return $results;
  }
  
}

?>