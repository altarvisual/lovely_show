<?php

class Trade_Model extends TinyMVC_Model
{

  function get_all_trades($brokerid)
  {
  	$results = array();
  	$this->db->query('select t.*,
					CASE buysell 
					  WHEN \'B\' THEN qty*price*100*buy_fee
					  WHEN \'S\' THEN qty*price*100*sell_fee
					end as fee
					 from trade t, broker b
					where t.broker_id=b.broker_id and t.broker_id=?',array($brokerid));
  	while($row = $this->db->next())
  		$results[] = $row;
  	return $results;
  }
  
  function get_sum_buy($broker_id)
  {
  	return $this->db->query_one('select sum(price*qty*100) buy_total from trade
								where buysell=\'B\'
  								and broker_id=?',array($broker_id));
  }
  
  function get_sum_sell($broker_id)
  {
  	return $this->db->query_one('select sum(price*qty*100) sell_total from trade
								where buysell=\'S\'
  								and broker_id=?',array($broker_id));
  }
  
  function get_sum_fee($broker_id)
  {
  	return $this->db->query_one('select sum(fee) fee_total from
								(
								select t.*,
								CASE buysell 
								  WHEN \'B\' THEN qty*price*100*buy_fee
								  WHEN \'S\' THEN qty*price*100*sell_fee
								end as fee
								 from trade t, broker b
								where t.broker_id=b.broker_id
								and t.broker_id=?) a',array($broker_id));
  }
  
}

?>