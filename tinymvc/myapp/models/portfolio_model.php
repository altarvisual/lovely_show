<?php

class Portfolio_Model extends TinyMVC_Model
{
  function get_portfolio($brokerid)
  {
  	$results = array();
    $this->db->query('select stock, qty_b-qty_s qty from (
						select a.stock, a.qty qty_b, b.qty qty_s from (
							select stock, sum(qty) qty, buysell from trade
							where buysell=?
    						and broker_id=?
							group by stock
							)a
						join
							(
								select stock, sum(qty) qty, buysell from trade
								where buysell=?
    							and broker_id=?
								group by stock
							) b
						on a.stock=b.stock
					) abc
					where qty_b > qty_s
					union
					select stock,sum(qty) from trade t1
					where buysell=?
					and not exists (select 1 from trade t2 where t2.buysell=? and t1.stock=t2.stock and broker_id=?)
    				and broker_id=?
					group by stock',array('B',$brokerid,'S',$brokerid,'B','S',$brokerid,$brokerid));
    while($row = $this->db->next())
      $results[] = $row;
    return $results;
  }
  
  function get_buy_trade($stock)
  {
    $this->db->query('select stock,trade_date,qty,price from trade
					where stock = ?
					and buysell = ?
					order by trade_date desc',array($stock,'B'));
    while($row = $this->db->next())
      $results[] = $row;
    return $results;
  }
  
  function get_portfolio_avg_price($brokerid){
	$portfolios = $this->get_portfolio($brokerid);
	if(!empty($portfolios)) {
		foreach($portfolios as $i => $portfolio) {
			$buyTrades = $this->get_buy_trade($portfolio['stock']);
			$qty = 0;
			$pricelot = 0;
			foreach($buyTrades as $buyTrade) {
				if($qty + $buyTrade['qty'] >= $portfolio['qty'] ) {
					$avgPrice = ($pricelot + ($portfolio['qty'] - $qty) * $buyTrade['price']) / $portfolio['qty'];
					$portfolios[$i]['date'] = $buyTrade['trade_date'];
					break;
				} else {
					$qty = $qty + $buyTrade['qty'];
					$pricelot = $pricelot + ($buyTrade['qty']*$buyTrade['price']);
				}
			}
			$portfolios[$i]['avg_price'] = round($avgPrice,2);
		}
	}
	 
	return $portfolios;
  }
  
  function get_portfolio_value($brokerid){
	$portfolios = $this->get_portfolio_avg_price($brokerid);
	$value = 0;
	foreach($portfolios as $portfolio) {
		$value = $value + ($portfolio['avg_price'] * $portfolio['qty'] * 100);
	}  
	return $value;
  }
  
  function get_last_trade($brokerid) {
  	return $this->db->query_one('select max(trade_date) date from trade where broker_id=?',array($brokerid));
  }
  
}

?>