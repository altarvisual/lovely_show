<?php

class TinyMVC_Library_Youtube {
	
	function upload($targetfile) {
		echo $targetfile;
		// Call set_include_path() as needed to point to your client library.
		set_include_path($_SERVER['DOCUMENT_ROOT'] . '/lovely/tinymvc/myfiles/plugins/google-api-php-client-master/src/');
		// echo $_SERVER['DOCUMENT_ROOT'] . '/lovely/tinymvc/myfiles/plugins/google-api-php-client-master/src/';
// 		session_start();
		require_once 'Google/autoload.php';
		require_once 'Google/Client.php';
		require_once 'Google/Service/YouTube.php';
		/*
		 * You can acquire an OAuth 2.0 client ID and client secret from the
		 * {{ Google Cloud Console }} <{{ https://cloud.google.com/console }}>
		 * For more information about using OAuth 2.0 to access Google APIs, please see:
		 * <https://developers.google.com/youtube/v3/guides/authentication>
		 * Please ensure that you have enabled the YouTube Data API for your project.
		 */
		$OAUTH2_CLIENT_ID = '1088801440873-6ujm5up7qdfoc5ttlg5r1lqq9fqlsr5l.apps.googleusercontent.com';
		$OAUTH2_CLIENT_SECRET = 'omICIq8iaTxMesrvCjrD664n';
		$REDIRECT = 'http://localhost/lovely/tinymvc/myfiles/plugins/google-api-php-client-master/examples/youtube.php';
		$APPNAME = "XXXXXXXXX";
		
		$client = new Google_Client();
		$client->setClientId($OAUTH2_CLIENT_ID);
		$client->setClientSecret($OAUTH2_CLIENT_SECRET);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		$client->setRedirectUri($REDIRECT);
		$client->setApplicationName($APPNAME);
		$client->setAccessType('offline');
		
		
		// Define an object that will be used to make all API requests.
		$youtube = new Google_Service_YouTube($client);
		
		if (isset($_GET['code'])) {
			if (strval($_SESSION['state']) !== strval($_GET['state'])) {
				die('The session state did not match.');
			}
		
			$client->authenticate($_GET['code']);
			$_SESSION['token'] = $client->getAccessToken();
		
		}
		
		if (isset($_SESSION['token'])) {
			$client->setAccessToken($_SESSION['token']);
			echo '<code>' . $_SESSION['token'] . '</code>';
		}
		
		// Check to ensure that the access token was successfully acquired.
		if ($client->getAccessToken()) {
			// 	$videoPath = "C:/Users/LENOVO-1/Desktop/testfile.mp4";
			$target_dir = $_SERVER['DOCUMENT_ROOT'] . '/lovely/uploads/';
			$target_file = $target_dir . $targetfile;
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
			$videoPath = $target_file;
			try {
				// 		// Call the channels.list method to retrieve information about the
				// 		// currently authenticated user's channel.
				// 		$channelsResponse = $youtube->channels->listChannels('contentDetails', array(
				// 				'mine' => 'true',
				// 		));
		
				// 		$htmlBody = '';
				// 		foreach ($channelsResponse['items'] as $channel) {
				// 			// Extract the unique playlist ID that identifies the list of videos
				// 			// uploaded to the channel, and then call the playlistItems.list method
				// 			// to retrieve that list.
				// 			$uploadsListId = $channel['contentDetails']['relatedPlaylists']['uploads'];
		
				// 			$playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet', array(
				// 					'playlistId' => $uploadsListId,
				// 					'maxResults' => 50
				// 			));
		
				// 			$htmlBody .= "<h3>Videos in list $uploadsListId</h3><ul>";
				// 			foreach ($playlistItemsResponse['items'] as $playlistItem) {
				// 				$htmlBody .= sprintf('<li>%s (%s)</li>', $playlistItem['snippet']['title'],
				// 						$playlistItem['snippet']['resourceId']['videoId']);
				// 			}
				// 			$htmlBody .= '</ul>';
				// 		}
		
				// REPLACE this value with the path to the file you are uploading.
				//$videoPath = "/path/to/file.mp4";
		
				// Create a snippet with title, description, tags and category ID
				// Create an asset resource and set its snippet metadata and type.
				// This example sets the video's title, description, keyword tags, and
				// video category.
				$snippet = new Google_Service_YouTube_VideoSnippet();
				$snippet->setTitle("Test title");
				$snippet->setDescription("Test description");
				$snippet->setTags(array("tag1", "tag2"));
		
				// Numeric video category. See
				// https://developers.google.com/youtube/v3/docs/videoCategories/list
				$snippet->setCategoryId("22");
		
				// Set the video's status to "public". Valid statuses are "public",
				// "private" and "unlisted".
				$status = new Google_Service_YouTube_VideoStatus();
				$status->privacyStatus = "public";
		
				// Associate the snippet and status objects with a new video resource.
				$video = new Google_Service_YouTube_Video();
				$video->setSnippet($snippet);
				$video->setStatus($status);
		
				// Specify the size of each chunk of data, in bytes. Set a higher value for
				// reliable connection as fewer chunks lead to faster uploads. Set a lower
				// value for better recovery on less reliable connections.
				$chunkSizeBytes = 1 * 1024 * 1024;
		
				// Setting the defer flag to true tells the client to return a request which can be called
				// with ->execute(); instead of making the API call immediately.
				$client->setDefer(true);
		
				// Create a request for the API's videos.insert method to create and upload the video.
				$insertRequest = $youtube->videos->insert("status,snippet", $video);
		
				// Create a MediaFileUpload object for resumable uploads.
				$media = new Google_Http_MediaFileUpload(
						$client,
						$insertRequest,
						'video/*',
						null,
						true,
						$chunkSizeBytes
				);
				$media->setFileSize(filesize($videoPath));
		
		
				// Read the media file and upload it chunk by chunk.
				$status = false;
				$handle = fopen($videoPath, "rb");
				while (!$status && !feof($handle)) {
					$chunk = fread($handle, $chunkSizeBytes);
					$status = $media->nextChunk($chunk);
				}
		
				fclose($handle);
		
				// If you want to make other calls after the file upload, set setDefer back to false
				$client->setDefer(false);
		
		
				$htmlBody .= "<h3>Video Uploaded</h3><ul>";
				$htmlBody .= sprintf('<li>%s (%s)</li>',
						$status['snippet']['title'],
						$status['id']);
		
				$htmlBody .= '</ul>';
		
			} catch (Google_ServiceException $e) {
				$htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
						htmlspecialchars($e->getMessage()));
			} catch (Google_Exception $e) {
				$htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
						htmlspecialchars($e->getMessage()));
			}
		
			$_SESSION['token'] = $client->getAccessToken();
			// 	header("Location: http://localhost/lovely/template/index.php/dancecontest/upload");
			// 	die();
		} else {
			$state = mt_rand();
			$client->setState($state);
			$_SESSION['state'] = $state;
			$authUrl = $client->createAuthUrl();
			echo "<h3>Authorization Required</h3>" ;
			echo "<p>You need to <a href='".$authUrl."'>authorise access</a> before proceeding.<p> ";
// 			$htmlBody = //<<<END
// 	  <h3>Authorization Required</h3>
// 	  <p>You need to <a href="$authUrl">authorise access</a> before proceeding.<p>
// 	END;
			}
		}
	}
	
?>
 
<!doctype html>
<html>
<head>
    <title>My Uploads</title>
</head>
<body>
 <?php// echo $htmlBody?>
</body>
</html>