<?php
// set_include_path($_SERVER['DOCUMENT_ROOT'] . '/lovely/tinymvc/myfiles/plugins/Facebook/');
// require_once 'autoload.php';
require_once( 'Facebook/FacebookSession.php' );
require_once( 'Facebook/FacebookRedirectLoginHelper.php' );
require_once( 'Facebook/FacebookRequest.php' );
require_once( 'Facebook/FacebookResponse.php' );
require_once( 'Facebook/FacebookSDKException.php' );
require_once( 'Facebook/FacebookRequestException.php' );
require_once( 'Facebook/FacebookAuthorizationException.php' );
require_once( 'Facebook/GraphObject.php' );
require_once( 'Facebook/Entities/AccessToken.php' );
require_once( 'Facebook/HttpClients/FacebookHttpable.php' );
require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php' );
require_once( 'Facebook/HttpClients/FacebookCurl.php' );

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

class TinyMVC_Library_Facebook {
	
	function login($video_id) {
		if(!isset($_SESSION)){
		    session_start();
		}
		// init app with app id and secret
		FacebookSession::setDefaultApplication( '1436356323330822','4317629ff91618e781ba33c1fb5a0512' );
		// login helper with redirect_uri
		$helper = new FacebookRedirectLoginHelper('http://'.$_SERVER['HTTP_HOST'].'/index.php/login/dologin_fb');
		try {
			$session = $helper->getSessionFromRedirect();
		} catch( FacebookRequestException $ex ) {
			// When Facebook returns an error
		} catch( Exception $ex ) {
			// When validation fails or other local issues
		}
		// see if we have a session
		if ( isset( $session ) ) {
			// graph api request for user data
			$request = new FacebookRequest( $session, 'GET', '/me' );
			$response = $request->execute();
			// get response
			$graphObject = $response->getGraphObject();
			$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
			$fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name
			$femail = $graphObject->getProperty('email');    // To Get Facebook email ID
			/* ---- Session Variables -----*/
// 			$_SESSION['FBID'] = $fbid;
			$_SESSION['fullname'] = $fbfullname;
			$_SESSION['email'] =  $femail;
			/* ---- header location after session ----*/
			header('Location: http://'.$_SERVER['HTTP_HOST'].'/dancecontest/dolike_fb');
		} else {
			$loginUrl = $helper->getLoginUrl(array(
					'scope' => 'email'
			));
			header("Location: ".$loginUrl);
		}
	}
}

?>