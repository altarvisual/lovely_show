<?php include 'header.php';?>
<body class="one-page">
	<!-- <div class="loader">
		<span class="loader1 block-loader"></span> <span
			class="loader2 block-loader"></span> <span
			class="loader3 block-loader"></span>
	</div> -->
	<!--header-->
	<section id="top" class="section slide-section slide-home scroll-texts">
		<div class="mobile-event"></div>
		<header class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-11 col-md-offset-1">
						<div class="top">
							<div class="btn-menu"></div>
							<!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div>
						<!--/.top-->
					</div>
				</div>
			</div>
		</header>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="titlebox">
						<div class="sub-title">
							<h2 class="lauren">Video Gallery</h2>
						</div>
						<!--/.sub-title-->
					</div>
					<!--/.titlebox-->
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<span class="container-left">
						<form action="#" >
							<div class="form-inline">
							<select class="form-control" name="city" id="citySrc">
									<option value="all">Semua Kota</option>
									<option value="bandung">Bandung</option>
									<option value="medan">Medan</option>
									<option value="makassar">Makassar</option>
									<option value="joglosemar">Joglosemar</option>
									<option value="surabaya">Surabaya</option>
								</select>
							<input class="form-control" type="text" id="search" name="search" placeholder="Search" size="35" onkeypress="handle(event)" value="<?=$name?>">
							</div>
						</form>
						
					</span>
				</div>
				<div class="col-md-6">
					<span class="container-right text-dark">Sort by: 
					<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/gallery/latest/1/all">Latest</a>
						&nbsp; 
					<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/gallery/viewed/1/all">Most Viewed</a> 
						&nbsp; 
					<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/gallery/liked/1/all">Most Liked</a>
						&nbsp; 
				
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="container">
						<div class="row">
							<?php foreach($videos as $video){
										?>
                                        <div class="col-sm-4 video-box">
								<div class="item-img">
									<a title="<?=$video['title']?>"
										href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/video/<?=$video['video_id']?>"><img
										src="http://img.youtube.com/vi/<?=$video['youtube_link']?>/0.jpg"
										class="img-responsive wp-post-image"
										alt="<?=$video['title']?>" height="240" width="360"></a>
									<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/video/<?=$video['video_id']?>"><div class="img-hover"></div></a>
								</div>
								<div class="feat-item">
									<div class="feat-info video-info-2360">
										<div class="meta">
											<span class="date"><?=$video['upload_date']?></span> 
											<span class="views"><i class="fa fa-eye"></i><?=$video['hits']?></span>
											<span class="heart"><i class="fa fa-heart"></i>
											<?php echo $video['total_like'] ?>
											
											</span> 
											<span class="fcomments"><i class="fa fa-comments"></i>6</span>

										</div>
										
										<h3>
											<a title="<?=$video['title']?>"
												href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/video/<?=$video['video_id']?>"><?=$video['title']?></a>
										</h3>
									</div>

								</div>

							</div>
										<?php }
										?>
						
						</div>
					</div>
					<!-- /#carousel-featured -->
				</div>
			</div>
			<br/><br/>
			<div class="row">
				<div class="col-md-12">
					<ul class="pagination pagination-lg">
					<?php 
					
					
					
						for($i=1 ; $i<=$total_page ; $i++) {
						
							if($city == "") {
								$url1 = $url.$i;
							} else {
								if($name != "")
									$url1 = $url.$i.'/'.$city.'/'.$name;
								else 
									$url1 = $url.$i.'/'.$city;
							}
					?>
						<li><a href="<?=$url1?>"><?=$i?></a></li>
						
					<?php 
					
					}?>
					</ul>
				</div>
			</div>

		</div>
<?php include 'footer.php';?>
	</section>
	<!--/.header-section-->
   
  	<script>
	    function handle(e){
	        if(e.keyCode === 13){
	        	e.preventDefault();
	        	var city = $('#citySrc').val();
	        	var search = $('#search').val();
	        	window.location.replace("http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/gallery/latest/1/"+city+"/"+search);
	        }
	        return false;
	    }
	</script>

   
   <?php include 'footer_js.php';?>
   
</body>

</html>