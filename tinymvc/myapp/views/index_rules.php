<?php include 'header.php';?>
<body class="one-page" class>
   <!-- <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div> -->
   <!--header-->
   <section id="top" class="section slide-section slide-home ">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
         <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     <h2 class="lauren">Terms & Conditions</h2>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
            </div>
         </div>
         <div class="row">
            	<div class="col-md-12">
                  <div class="box-container howjoin tc">
					VIDEO DANCE CONTEST
					  adalah rangkaian dari program PUTERI LOVELY SHOW milik MUSTIKA PUTERI
					  yang berlangsung dari tanggal 22 April 2015 - 15 Agustus 2015.
					  Kompetisi ini mengajak kamu untuk membuat video dance cover JKT48
					  sekreatif mungkin.<br>Program PUTERI LOVELY
					  SHOW merupakan progam roadshow yang akan berlangsung di kota Bandung,
					  Medan, Joglosemar (Yogyakarta, Solo, Semarang), Makassar dan Surabaya
					dengan JKT48 sebagai brand ambassador.<br><br>
					<b>VIDEO DANCE CONTEST</b><br>
					
					<ul>
					  <li>KRITERIA PESERTA  </li>
					    <ol>
					      <li>Remaja Puteri usia 13 - 19 tahun</li>
					      <li>Grup kompetisi terdiri dari 2-5
					        orang.</li>
					      <li>Warga Negara Indonesia yang
					        berdomisili di kota Medan, Bandung, Semarang, Solo, Yogyakarta,
					        Surabaya, Makassar dan kota-kota disekitarnya.</li>
					    </ol>
					</ul>
					
					<ul>
					  <li>PERIODE KOMPETISI</li>
					  	<ol>
					    	<li>Periode kompetisi Video Dance
					      Contest akan dibagi dalam 5 daerah, yaitu :
					      	</li>
					  	</ol>
						<ol>
					  		<ol>
					        <li>Bandung 22 April 2015 - 12 Mei
					          2015</li>
					        <li>Medan 28 April 2015 - 18 Mei
					          2015</li>
					        <li>Joglosemar (Yogyakarta, Semarang,
					          Solo)  4 Mei - 24 Mei 2015</li>
					        <li>Makassar 5 Mei 2015 - 25 Mei
					          2015</li>
					        <li>Surabaya 15 Juni 2015 - 11
					          Agustus 2015</li>
					  		</ol>
						</ol>
					</ul>
					
					<ul>
					  <li>MEKANISME KOMPETISI    
					    <ol>
					      <li>Cara Mengikuti Kompetisi<br>Untuk
					        mengikuti kompetisi VIDEO DANCE CONTEST, ini langkah - langkah yang
					        harus kamu lakukan :
					      </li>
					    </ol>
					  </li>
						<ol>
					  		<ol>
					        <li>Gunakan jingle MUSTIKA PUTERI yang
					          bisa didownload di <a href="http://www.puterilovelyshow.com/dancecontest/download" target="_blank" >www.puterilovelyshow.com/<wbr>dancecontest/download</a>
					          sebagai backsound wajib.    
					        </li>
					        <li>Buat Video Dance Cover kamu sesuai
					          lagu tersebut, baik merekam sendiri dengan handycam/handphone/kamera
					          video professional atau melalui booth PUTERI LOVELY SHOW yang ada di
					          sekolah kamu (lihat list roadshow PUTERI LOVELY SHOW) di
					          masing-masingkota.    </li>
					        <li>Buka microsite
					          <font color="#000000"><a href="http://www.puterilovelyshow.com/" target="_blank">www.puterilovelyshow.com</a></font>
					          dan pilih menu upload video.</li>
					        <li>Pilih kota sesuai daerah domisili
					          terdekat kamu (Bandung, Medan, Joglosemar, Makassar, Surabaya)</li>
					        <li>Sistem akan langsung menghubungkan
					          dengan Youtube Account, jika sudah mempunyai Youtube Account ikuti
					          langkah selanjutnya dan apabila belum mempunyai diharapkan untuk
					          membuat Youtube Account terlebih dahulu.</li>
					        <li>Isi data pribadi salah satu
					          personal grup (<i>leader</i>) sebagai perwakilan dari grup.</li>
					        <li>Upload video dance cover MUSTIKA
					          PUTERI &amp; JKT48 dengan format sebagai berikut :
					          
					        </li>
					    	</ol>
						</ol>
					            <div style="margin-left:0.85in;margin-bottom:0in"><b>Judul </b>
					          </div>
					              <p style="margin-left:0.85in;text-indent:0in;margin-bottom:0in"> [PUTERI
					                LOVELY SHOW] - Nama Grup*</p>
					            
					          
					          
					            <div style="margin-left:0.85in;margin-bottom:0in"><b>Deskrispi</b> :</div>
					            
					              <div style="margin-left:0.85in">VIDEO DANCE
					                CONTEST merupakan rangkaian program PUTERI LOVELY SHOW yang berjalan
					                di kota Bandung, Medan, Joglosemar (Yogyakarta, Solo, Semarang),
					                Makassar, Surabaya. Kompetisi ini mengajak kamu untuk membuat video
					                dance cover JKT48 menggunakan backsound jingle MUSTIKA PUTERI.
					                Pemenang akan berkesempatan untuk Private Lunch bareng JKT48 atau
					                sebagai pembuka di konser PUTERI LOVELY SHOW JKT48. Langsung saja
					                kunjungi microsite <font color="#000000"><a href="http://www.puterilovelyshow.com/" target="_blank">www.puterilovelyshow.com</a></font> atau follow terus Twitter @MustikaPuteriID dan Facebook Fan
					                Page Mustika Puteri #PuteriLovelyShow</div>
					              <div style="margin-left:0.85in;margin-bottom:0in">Nama Anggota Grup*</div>
					            
					            <div style="margin-left:0.85in;margin-bottom:0in">(*) ditulis
					              sesuai dengan Nama Grup &amp; Nama Anggota Grup </div>
					         
					  			
					
					    <ol start="8">
					      <ol start="8">
					        <li><span style="margin-bottom:0in">Kemudian klik submit video.
					        </span></li>
					        <li><span style="margin-bottom:0in"> Setelah melewati proses seleksi,
					          video akan ditampilkan di halaman <i>gallery</i> video <i>microsite</i> <font color="#000000"><a href="http://www.puterilovelyshow.com/" target="_blank">www.puterilovelyshow.com</a></font></span></li>
					        <li>MustikaPuteri akan mengumumkan
					          pemenang selambat-lambatnya 2 hari setelah periode kompetisi di
					          masing-masing daerah berakhir. Pemenang akan diumumkan melalui <i>microsite</i> <font color="#000000"><a href="http://www.puterilovelyshow.com/" target="_blank">www.puterilovelyshow.com</a></font>,
					          Facebook Fan Page Mustika Puteri dan Twitter @MustikaPuteriID.
					        </li>
					      </ol>
					    </ol>
					
					
					<ol start="2"><li><i>Backsound </i>video <i>dance</i>
					      <br>
					      Jingle MUSTIKA PUTERI
					      bisa didownload di <a href="http://www.puterilovelyshow.com/dancecontest/download" target="_blank">www.puterilovelyshow.com/<wbr>dancecontest/download</a></li>
					  </ol>
					</ul>
					
					<ul>
					  <li>KETENTUAN VIDEO
					    
					    <ol>
					      <li>Video dance cover  wajib memakai
					        jingle MUSTIKA PUTERI sebagai <i>backsound</i> video.  </li>
					      <li>Peserta dapat mengkreasikan
					        sendiri koreografi dance yang mereka buat. </li>
					      <li>Akun perwakilan grup peserta hanya
					        dapat mengirimkan video 1 kali dan peserta dapat menggunakan akun
					        lain jika ingin mengirimkan video lebih dari 1 kali.</li>
					      <li>Peserta dilarang menampilkan video
					        yang mengandung unsur SARA, politik, pornografi, sadism sertahal-hal
					        yang bertentangan dengan nilaikepautan dan kesusilaan.</li>
					      <li>MUSTIKA PUTERI berhak untuk
					        menghapus video yang mengandungunsur SARA, politik, pornografi,
					        sadisme, serta hal lainnya yang bertentangan dengan norma-norma dan
					        peraturan yang berlaku di masyarakat, tanpa pemberitahuan terlebih
					        dahulu.</li>
					      <li>Video yang diikutsertakan
					        merupakan karya asli.      </li>
					      <li>Pihak MUSTIKA PUTERI berhak
					        mendiskualifikasi peserta yang melanggar hakcipta serta syarat dan
					        ketentuan lainnya.<br>
					      </li>
					    </ol>
					  </li>
					</ul>
					
					<ul>
					  <li>KETENTUAN PEMENANG
					    <ol>
					      <li>Hadiah Utama<br>
					        Hadiah utama akan
					        dibagi berdasarkan daerah.  
					      </li>
						<ol>
					        	<li>Pihak MUSTIKA PUTERI akan memilih
					          	1 (satu) grup pemenang utama dari setiap daerah, yaitu :          </li>
					        	<ol>
					        		
					                  <li>Untuk daerah <b>Bandung </b>dan
					                    sekitarnya, pemenang utama akan mendapatkan kesempatan untuk
					                    "Private Lunch with JKT48"<i> , invitation</i> "Pajama Party
					                    with JKT48" yang akan berlangsung di kota Bandung dan uang tunai
					                    sebesar Rp 1.500.000,-</li>
					                  <li>Untuk daerah <b>Medan </b>dan
					                    sekitarnya, pemenang utama akan mendapatkan kesempatan untuk
					                    "Private Lunch with JKT48", <i>invitation</i> "Pop Star Party
					                    with JKT48" yang akan berlangsung di kota Medan dan uang tunai
					                    sebesar Rp 1.500.000,-.</li>
					                  <li>Untuk daerah <b>Makassar </b>dan
					                    sekitarnya, pemenang utama akan mendapatkan kesempatan untuk
					                    "Private Lunch with JKT48", <i>invitation</i> "Summer Holiday
					                    Party with JKT48" yang akan berlangsung di kota Makassar dan uang
					                    tunai sebesar Rp 1.500.000,-.</li>
					                  <li>Untuk daerah <b>Yogyakarta, Solo,
					                    Semarang </b>dan sekitarnya, pemenang utama akan mendapatkan
					                    kesempatan untuk "Private Lunch with JKT48", <i>invitation</i>
					                    "Tea Party with JKT48" yang akan berlangsung di kota Yogyakarta
					                    dan uang tunai sebesar Rp 1.500.000,-.</li>
					                  <li>Untuk daerah <b>Surabaya </b>dan
					                    sekitarnya, pemenang utama akan mendapatkan kesempatan untuk tampil
					                    sebagai <i>opening performer </i>dalam KONSER PUTERI LOVELY SHOW
					                    JKT48 yang akan berlangsung di kota Surabaya dan uang tunai sebesar
					                    Rp 1.500.000,-.
					                  </li>
									
					   </ol>
					    </ol>
					    	<li>Hadiah Hiburan<br>Hadiah hiburan akan
					      	dibagi berdasarkan daerah.  
					    	</li>
					    		<ol>
					      			
									<li>Pihak MUSTIKA PUTERI akan memilih
					                      2 (dua) grup pemenang hiburan dari setiap daerah, yaitu :          </li>
					                    <ol>
					                      <li>Untuk daerah <b>Bandung </b>dan
					                        sekitarnya, pemenang hiburan akan mendapatkan <i>invitation</i>
					                        "Pajama Party with JKT48" yang akan berlangsung di kota Bandung
					                        dan uang tunai sebesar Rp 1.000.000,- (pemenang hiburan pertama) Rp
					                        500.000,- (pemenang hiburan kedua).</li>
					                      <li>Untuk daerah <b>Medan </b>dan
					                        sekitarnya, pemenang hiburan akan mendapatkan <i>invitation</i>
					                        "Popstar Party with JKT48" yang akan berlangsung di kota Medan
					                        dan uang tunai sebesar Rp 1.000.000,- (pemenang hiburan pertama) Rp
					                        500.000,- (pemenang hiburan kedua).</li>
					                      <li>Untuk daerah <b>Makassar </b>dan
					                        sekitarnya, pemenang hiburan akan mendapatkan <i>free </i>tiket
					                        "Summer Holiday Party with JKT48" yang akan berlangsung di kota
					                        Makassar dan uang tunai sebesar Rp 1.000.000,- (pemenang hiburan
					                        pertama) Rp 500.000,- (pemenang hiburan kedua).</li>
					                      <li>Untuk daerah <b>Yogyakarta, Solo,
					                        Semarang </b>dan sekitarnya, pemenang hiburan akan mendapatkan <i>free
					                          </i>tiket "Tea Party with JKT48" yang akanberlangsung di kota
					                        Yogyakarta dan uang tunai sebesar Rp 1.000.000,- (pemenang hiburan
					                        pertama) Rp 500.000,- (pemenang hiburan kedua).</li>
					                      <li>Untuk daerah <b>Surabaya </b>dan
					                        sekitarnya, pemenang hiburan akan mendapatkan <i>free </i>tiket
					                        masuk untuk menonton KONSER PUTERI LOVELY SHOW JKT48 di kota
					                        Surabaya dan uang tunai sebesar Rp 1.000.000,- (pemenang hiburan
					                        pertama) Rp 500.000,- (pemenang hiburan kedua).</li>
					        			
					      			</ol>
					    </ol>
					    <li>Pihak MUSTIKA PUTERI tidak
					      menanggung segala biaya akomodasi pemenang utama dan pemenang
					      hiburan untuk menuju ke lokasi acara.</li>
					    <li>Pajak hadiah ditanggung oleh pihak
					      MUSTIKA PUTERI.</li>
					    <li>Pemenang hadiah utama dan hadiah
					      hiburan akan diumumkan selambat-lambatnya 2 hari setelah periode
					      kompetisi di masing-masing daerah berakhir.</li>
					    <li>Pengumuman pemenang akan dilakukan
					      melalui <i>microsite</i> <font color="#000000"><a href="http://www.puterilovelyshow.com/" target="_blank">www.puterilovelyshow.com</a></font>,
					      Facebook Fan Page Mustika Puteri dan Twitter @MustikaPuteriID.</li>
					    <li>Pihak MUSTIKA PUTERI akan
					      melakukan proses <i>verifikasi</i> terhadap setiap pemenang melalui
					      alamat email akun perwakilan grup dan pemenang diharapkan
					      melampirkan identitas diri (KTP/SIM/PASPOR,dsb)seluruh anggota grup
					      untuk proses verifikasi.</li>
					    <li>Video akan dinilai berdasarkan :
					    </li>
					    <ol>
					      
					        <li>Pemenang Utama          </li>
					        <ol>
					          <li>Pemenang utama dipilih berdasarkan
					            kreativitas dalam melakukan dance cover MUSTIKA PUTERI Pemenang utama dipilih berdasarkan
					            kreativitas dalam melakukan dance cover MUSTIKA PUTERI &amp; JKT48.</li>
					          <li>Pemenang utama akan dipilih
					            melalui penilaian dewan juri yang terdiri dari pihak MUSTIKA PUTERI
					            Pemenang utama akan dipilih
					            melalui penilaian dewan juri yang terdiri dari pihak MUSTIKA PUTERI
					            &amp; JKT48.</li>
					        </ol>
					        
					        <li>Pemenang Hiburan          </li>
					        <ol>
					          <li>Pemenang hiburan akan dipilih
					            berdasarkan <i>LIKE</i> TERBANYAK yang tampil di tiap video gallery
					            <i>microsite</i> <font color="#000000"><a href="http://www.puterilovelyshow.com/" target="_blank">www.puterilovelyshow.com</a></font></li>
					          <li>Sistem dari
					            <i>microsite</i> <font color="#000000"><a href="http://www.puterilovelyshow.com/" target="_blank">www.puterilovelyshow.com</a></font>
					            yang akan menghitung jumlah <i>LIKE </i>tiap video.</li>
					        
					        </ol>
					</ol>
					
					    <li>Keputusan pemenang oleh pihak
					      MUSTIKA RATU bersifat mutlak dan tidak dapat diganggu-gugat. Tidak
					      ada korenspondensi terkait hal ini.</li>
					</ol>
					</li>
					</ul>
					<ul>
					  <li>
					    UMUM
					    <ol>
					      <li>Setiap video yang telah dikirimkan
					        ke kompetesi VIDEO DANCE CONTEST serta <i>microsite</i>
					        <font color="#000000"><a href="http://www.puterilovelyshow.com/" target="_blank">www.puterilovelyshow.com</a></font>
					        akan menjadi milik MUSTIKA PUTERI dan PT. Mustika Ratu.Tbk. Pihak
					        MUSTIKA PUTERI dan PT. Mustika Ratu.Tbk berhak untuk menggunakan
					        video-video tersebut pada media promosi dalam rentang waktu yang
					        tidak terbatas.</li>
					      <li>Pihak MUSTIKA PUTERI berhak
					        mendiskualifikasi peserta yang melanggar syarat dan ketentuan dalam
					        kompetisi VIDEO DANCE CONTEST.</li>
					      <li>Pihak MUSTIKA PUTERI berhak untuk
					        memperbaharui hadiah dari kompetisi ini sewaktu-waktu.</li>
					      <li>Pihak MUSTIKA PUTERI berhak untuk
					        melakukan penambahan atau pengurangan syarat dan ketentuan yang
					        berlaku tanpa melakukan pemberitahuan terlebih dahulu.</li>
					      <li>Peserta sepenuhnya melindungi dan
					        membebaskan MUSTIKA PUTERI dari tanggung jawab atau tuntutan apapun
					        dari pihak ketiga manapun.</li>
					      <li>Hati-hati penipuan yang
					        mengatasnamakan panitia penyelenggara (MUSTIKA PUTERI).</li>
					      <li>Setiap peserta yang mengikuti
					        VIDEO DANCE CONTEST, dianggap telah membaca syarat dan ketentuan
					        ini.</li>
					      <li>Dengan membaca seluruh syarat dan
					        ketentuan di atas, peserta telah sepenuhnya mengerti dan menyetujui
					        untuk mengikuti kompetisi ini sesuai dengan persyaratan dan
					        ketentuan yang berlaku.</li>
					      <li>Untuk informasi lebih lanjut
					        mengenai kompetisi VIDEO DANCE CONTEST dan program PUTERI LOVELY
					        SHOW dapat mengirimkan pertanyaan ke Facebook Fan Page
					        MustikaPuteri/Twitter @MustikaPuteriID/Consumer Contact MUSTIKA
					        PUTERI +6221 8312323.</li>
					    </ol>
					  </li>
					</ul>
               	  </div>
               	</div>
            </div>
      </div>
      <?php include 'footer.php';?>
   </section><!--/.header-section-->

   	
   	<?php include 'footer_js.php';?>
   
</body>

</html>