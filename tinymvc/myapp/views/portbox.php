<div class="portBox pink" id="roadshow">
		<div id="sh_carousel" class="carousel slide">
    <!-- Carousel indicators -->
	<div class="join-container bg-white">
    <div class="carousel-indicators">
      <div style="visibility: visible; animation-name: fadeInUp;" class="join-pics wow fadeInUp btn eventime_button graybtn graytxt animated active" data-target="#sh_carousel" data-slide-to="0">
         Bandung
      </div>
      <div style="visibility: visible; animation-name: fadeInDown;" class="join-pics wow fadeInDown btn eventime_button graybtn graytxt animated" data-target="#sh_carousel" data-slide-to="1">
         Medan
      </div>
      <div style="visibility: visible; animation-name: fadeInUp;" class="join-pics wow fadeInUp btn eventime_button graybtn graytxt animated" data-target="#sh_carousel" data-slide-to="2">
         Makassar
      </div>
      <div style="visibility: visible; animation-name: fadeInDown;" class="join-pics wow fadeInDown btn eventime_button graybtn graytxt animated" data-target="#sh_carousel" data-slide-to="3">
         Joglosemar
      </div>
	  <div style="visibility: visible; animation-name: fadeInDown;" class="join-pics wow fadeInDown btn eventime_button graybtn graytxt animated" data-target="#sh_carousel" data-slide-to="4">
         Surabaya
      </div>
    </div>
	</div>
    <!-- Carousel items -->
    <div class="carousel-inner">
      <div class="item active">
        <!-- DAY ONE -->
       
        <!-- Accordion item -->
       
        <div id="accordion1">
		<div class="mini_gap">
          </div>
          <img alt="image"  src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/banner_bandung.png"> 
   	 	  <div class="mini_gap"></div>
   	 	  		
   	 	  
   	 	  <div class="row">
           <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					 27 April 2015
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMAN 1 BANDUNG
                  </div>
                  <div class="address">
                     Jl. Ir H djuanda Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					28 April 2015
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMAN 5 BANDUNG
                  </div>
                  <div class="address">
                     Jl. Belitung No. 8 Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
         
          </div>
          
          <div class="row">
          <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					29 April 2015
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMAN 8 BANDUNG
                  </div>
                  <div class="address">
                     Jl. Selontongan No. 3 Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
           
         <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMAN 2 BANDUNG
                  </div>
                  <div class="address">
                     Jl. Cihampelas Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
          </div>
          
          <div class="row">
           <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMAN 7 BANDUNG
                  </div>
                  <div class="address">
                     Jl. Lengkong Kecil No. 53 Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
         <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMAN 20 BANDUNG
                  </div>
                  <div class="address">
                     Jl. Citarum No. 23 Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
          </div>
          
          <div class="row">
           <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMAN 22 BANDUNG
                  </div>
                  <div class="address">
                     Jl. Rajamantri Kulon No. 17A Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
         <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMAN 3 BANDUNG
                  </div>
                  <div class="address">
                     Jl. Belitung No. 8 Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
          </div>
          
          <div class="row">
           <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                     SMA TARUNA BAKTI
                  </div>
                  <div class="address">
                     Jl.RE.Martadina No.52 Bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
         <div class="col-md-6">
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
          
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <div class="name">
                            SMA BPI                 
                  </div>
                  <div class="address">
                     Jl. Burang rang bandung
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
          </div>
   	 	  
           
		  
        </div>
        <!-- End of the day -->
      </div>
      <div class="item">
        <!-- DAY TWO -->
       
        <!-- Accordion item -->
        <div id="accordion2">
          <div class="mini_gap">
          </div>
          <img alt="image"  src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/comingsoon.png"> 
          <!-- End of the accordion -->
        </div>
        <!-- End of the day -->
      </div>
      <div class="item">
        
        <!-- Accordion item -->
        <div class="mini_gap">
        </div>
        <div id="accordion3">
          <img alt="image"  src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/comingsoon.png"> 
          <!-- End of the accordion -->
        </div>
        <!-- End of the day -->
      </div>
      <div class="item">
        
        <!-- Accordion item -->
        <div class="mini_gap">
        </div>
        <div id="accordion4">
         <img alt="image"  src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/comingsoon.png"> 
          <!-- End of the accordion -->
        </div>
        <!-- End of the day -->
      </div>
	  <div class="item">
        <!-- DAY TWO -->
       
        <!-- Accordion item -->
        <div id="accordion5">
          <div class="mini_gap">
          </div>
         <img alt="image"  src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/comingsoon.png"> 
          
          <!-- End of the accordion -->
        </div>
        <!-- End of the day -->
      </div>
      <!-- Carousel nav -->
     
      <div class="gap">
      </div>
    </div>
  </div>
</div>


<div class="portBox-join pink" id="howtojoin">
	<h2 align="center" class="text-white ">How to Join<img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/icons/khusus-puteri.png"></h2>
	<h5 align="center" class="text-white segoui-bold">Tunjukkan kretifitas kalian bikin video dance cover Mustika Puteri & JKT48</h5>
	<div class="join-container">
	<div class="join-pics"><img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/icons/step-1.png"><br/>
	<div class="text-white">Download lagu jingle <br/>Mustika Puteri di www.puterilovelyshow.com untuk backsound wajib video dance cover</div></div> 
	<div class="join-pics"><img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/icons/step-2.png"><br/>
	<div class="text-white">Bikin grup dengan anggota 2-5 orang dan record video dance cover Mustika Puteri & JKT48 sekreatif kalian</div></div> 
	<div class="join-pics"><img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/icons/step-3.png"><br/>
	<div class="text-white">Masuk ke www.puterilovelyshow.com dan ikut petunjuk untuk upload video</div> </div>
	<div class="join-pics"><img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/icons/step-4.png"><br/>
	<div class="text-white">Share video untuk mendapat dukungan dari teman-temanmu</div> </div>
	<div class="join-pics"><img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/icons/step-5.png"><br/>
	<div class="text-white">Menangkan kesempatan <br/>untuk private lunch JKT 48 / sebagai pembuka di konser JKT48 </div> </div>
	</div>
	
</div>



<div class="portBox pink" id="city">
		<h5 align="center" class="text-white">Pilih kotamu</h5>
           <a href="#" data-display="upload" data-closeBGclick="true" class="city-btn" id="Bandung">
           <img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/bandung.png"
           onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/bandung_hover.png'" 
           onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/bandung.png'" /></a> <br/>
           <a href="#" data-display="message-medan" data-closeBGclick="true" class="city-btn" id="Medan">
           <img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/medan.png"
           onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/medan_hover.png'" 
           onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/medan.png'" /></a> <br/>
           <a href="#" data-display="message-makassar" data-closeBGclick="true" class="city-btn" id="Makassar">
           <img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/makassar.png"
           onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/makassar_hover.png'" 
           onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/makassar.png'" /></a> <br/>
           <a href="#" data-display="message-joglosemar" data-closeBGclick="true" class="city-btn" id="Joglosemar">
           <img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/joglosemar.png"
           onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/joglosemar_hover.png'" 
           onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/joglosemar.png'" /></a> <br/>
           <a href="#" data-display="message-surabaya" data-closeBGclick="true" class="city-btn" id="Surabaya">
           <img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/surabaya.png"
           onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/surabaya_hover.png'" 
           onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/surabaya.png'" /></a> 
</div>

<script type="text/javascript">
	$('.city-btn').click(function(){
	 	var portBoxID = 'city';
		$('#'+portBoxID).css({'display' : 'none'});
		var id = $(this).attr('id');
	    $('#city-perform').text(id);
	});

</script>

<div class="portBox pink" id="message-medan">
		<h5 align="center" class="text-white">Siap-siap ya, kompetisi akan segera dimulai</h5>
		<h2 align="center" class="text-white">28 April - 18 Mei 2015</h2>
</div>

<div class="portBox pink" id="message-makassar">
		<h5 align="center" class="text-white">Siap-siap ya, kompetisi akan segera dimulai</h5>
		<h2 align="center" class="text-white">5 Mei - 25 Mei 2015</h2>
</div>

<div class="portBox pink" id="message-joglosemar">
		<h5 align="center" class="text-white">Siap-siap ya, kompetisi akan segera dimulai</h5>
		<h2 align="center" class="text-white">4 Mei - 24 Mei 2015</h2>
</div>

<div class="portBox pink" id="message-surabaya">
		<h5 align="center" class="text-white">Siap-siap ya, kompetisi akan segera dimulai</h5>
		<h2 align="center" class="text-white">22 Jul - 8 Agustus 2015</h2>
</div>


<script src="https://apis.google.com/js/client:platform.js?onload=render" async defer></script>
  <script>
  function render() {
    gapi.signin.render('customBtn', {
      'callback': 'oauth2Callback',
      'clientid': '1088801440873-fe5g05opnpca1p8kn7t6jcooo4p28mk8.apps.googleusercontent.com',
      'cookiepolicy': 'none',
      'scope': 'https://www.googleapis.com/auth/youtube.readonly https://www.googleapis.com/auth/youtube.upload https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
    });
  }
  </script>
  <style type="text/css">
    #customBtn {
      display: inline-block;
      background: #dd4b39;
      color: white;
      width: 165px;
      border-radius: 5px;
      white-space: nowrap;
      cursor:pointer;
    }
    
    #customBtn:hover {
      background: white;
      color: #dd4b39;
      cursor: hand;
    }
    span.label {
      font-weight: bold;
    }
    span.icon {
      background: url('/+/images/branding/btn_red_32.png') transparent 5px 50% no-repeat;
      display: inline-block;
      vertical-align: middle;
      width: 35px;
      height: 35px;
      border-right: #bb3f30 1px solid;
    }
    span.buttonText {
      display: inline-block;
      vertical-align: middle;
      padding-left: 35px;
      padding-right: 55px;
      padding-top: 20px;
      padding-bottom: 20px;
      font-size: 14px;
      font-weight: bold;
      /* Use the Roboto font that is loaded in the <head> */
      font-family: 'Roboto',arial,sans-serif;
    }
  </style>


<div class="portBox pink" id="upload">
	<div class="box-container ">
	
   <div id="gSignInWrapper" class="pre-sign-in">
    <div id="customBtn" >
      <span class="buttonText">Mulai Upload</span>
    </div>
    
  </div>
	
						
						
    <div class="post-sign-in">
      <!-- <div>
        <img id="channel-thumbnail">
        <span id="channel-name"></span>
      </div> -->

      <form id="upload-form">
      						<div class="form-group">
								<label>Kota Perform</label> <br/><span id="city-perform"></span>
							</div>
      						<div class="form-group">
								<label>Email </label><br/> 
								<input class="form-control" id="email"
									name="email" required>
							</div>
							<div class="form-group">
								<label>Nama </label><br/> 
								<input class="form-control" id="name"
									name="name" required>
							</div>
							<div class="form-group">
								<label>Alamat </label><br/> 
								<textarea class="form-control" rows="3" name="address" id="address" required></textarea>
							</div>
							<div class="form-group">
								<label>Telepon </label><br/> 
								<input class="form-control" id="telp"
									name="telp" required>
							</div>
							<div class="form-group">
								<label>Judul Video </label><br/> <div class="desc-form">[PUTERI LOVELY SHOW] - NamaGrup #PuteriLovelyShow </div> 
								<input class="form-control" id="title"
									name="title" required>
							</div>
							<div class="form-group">
								<label>Deskripsi</label><br/> <div class="desc-form">Nama anggota grup</div>
								<textarea class="form-control" rows="3" name="description" id="description" required></textarea>
							</div>
							<div class="form-group">
								<label>Nama Group/Sekolah</label> <input class="form-control"
									name="group" id="group" required>
							</div>
							<div class="form-group">
								<label>Upload Video</label> 
								<div class="form-inline">
								<input type="file" name="file" id="file" data-max-size="100000000" />
								</div>
							</div>
							<div class="form-group">
								<label>Captcha</label> 
								<div class="form-inline">
								<img src="http://<?php echo $_SERVER['HTTP_HOST'];?>/signup/captcha" /> <input class="form-control" required>
								</div>
							</div>
        <div class="myButton"><input id="submit" type="submit" name="" value=""></div>
      </form>

      <div class="during-upload">
        <p><span id="percent-transferred"></span>% done (<span id="bytes-transferred"></span>/<span id="total-bytes"></span> bytes)</p>
        <progress id="upload-progress" max="1" value="0"></progress>
      </div>

      <div class="post-upload">
        <p>Menunggu proses verifikasi youtube...</p>
        <ul id="post-upload-status"></ul>
        
      </div>
    </div>
						
						
					</div>
</div>

<div class="portBox pink-home" id="welcome">
	<img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/image_winner.png"> 
</div>

