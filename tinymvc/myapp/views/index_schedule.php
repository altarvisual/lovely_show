<?php include 'header.php';?>
<body class="one-page" >
   <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div>
   <!--header-->
   <section id="top" class="section slide-section slide-home ">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
         <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     <h2>Schedule</h2>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
               
            </div><!-- /.portfolio-container -->
            </div>
            <div id="schedule_menu">
 


  <div class="">
  <div id="sh_carousel" class="carousel slide">
    <!-- Carousel indicators -->
	<div class="col-md-12 bg-white">
    <div class="carousel-indicators">
      <div style="visibility: visible; animation-name: fadeInUp;" class="col-md-2 wow fadeInUp btn eventime_button graybtn graytxt animated active" data-target="#sh_carousel" data-slide-to="0">
         Medan
      </div>
      <div style="visibility: visible; animation-name: fadeInDown;" class="col-md-2 wow fadeInDown btn eventime_button graybtn graytxt animated" data-target="#sh_carousel" data-slide-to="1">
         Makassar
      </div>
      <div style="visibility: visible; animation-name: fadeInUp;" class="col-md-2 wow fadeInUp btn eventime_button graybtn graytxt animated" data-target="#sh_carousel" data-slide-to="2">
         Bandung
      </div>
      <div style="visibility: visible; animation-name: fadeInDown;" class="col-md-2 wow fadeInDown btn eventime_button graybtn graytxt animated" data-target="#sh_carousel" data-slide-to="3">
         Yogyakarta
      </div>
	  <div style="visibility: visible; animation-name: fadeInDown;" class="col-md-2 wow fadeInDown btn eventime_button graybtn graytxt animated" data-target="#sh_carousel" data-slide-to="4">
         Surabaya
      </div>
    </div>
	</div>
    <!-- Carousel items -->
    <div class="carousel-inner">
      <div class="item active">
        <!-- DAY ONE -->
       
        <!-- Accordion item -->
       
        <div id="accordion1">
		<div class="mini_gap">
          </div>
          <img alt="image"  src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/bg_lovelymood.jpg"> 
           <div class="mini_gap"></div>
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
		   <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
         
          <!-- Accordion item -->
          <div class="mini_gap">
          </div>
          
		  <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
		  
        </div>
        <!-- End of the day -->
      </div>
      <div class="item">
        <!-- DAY TWO -->
       
        <!-- Accordion item -->
        <div id="accordion2">
          <div class="mini_gap">
          </div>
		  <img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/bg_lovelymood.jpg"> 
           <div class="mini_gap"></div>
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
         
          <!-- Accordion item -->
          <div class="mini_gap">
          </div>
          
		  <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
          <!-- End of the accordion -->
        </div>
        <!-- End of the day -->
      </div>
      <div class="item">
        
        <!-- Accordion item -->
        <div class="mini_gap">
        </div>
        <div id="accordion3">
		<img alt="image"  src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/bg_lovelymood.jpg"> 
           <div class="mini_gap"></div>
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
         
          <!-- Accordion item -->
          <div class="mini_gap">
          </div>
          
		  <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
          <!-- End of the accordion -->
        </div>
        <!-- End of the day -->
      </div>
      <div class="item">
        
        <!-- Accordion item -->
        <div class="mini_gap">
        </div>
        <div id="accordion4">
		<img alt="image"  src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/bg_lovelymood.jpg"> 
           <div class="mini_gap"></div>
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
         
          <!-- Accordion item -->
          <div class="mini_gap">
          </div>
          
		  <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
          <!-- End of the accordion -->
        </div>
        <!-- End of the day -->
      </div>
	  <div class="item">
        <!-- DAY TWO -->
       
        <!-- Accordion item -->
        <div id="accordion5">
          <div class="mini_gap">
          </div>
		  <img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/bg_lovelymood.jpg"> 
           <div class="mini_gap"></div>
          <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
         
          <!-- Accordion item -->
          <div class="mini_gap">
          </div>
          
		  <div style="visibility: visible; animation-name: fadeInDown;" class="panel wow fadeInDown animated">
            <a data-toggle="collapse" data-parent="#accordion1" href="#one_two">
            <div class="accordion_left">
              <div class="accordions_titles">
                <p class="accordion_date">
					21 August
                </p>
              </div>
            </div>
            <div class="accordion_right">
              <div class="accordions_titles">
                <div class="accordion_description">
                  <p>
                     SMAN 40 Medan
                  </p>
                  <p class="name">
                     Jl. medan perkasa 50 no.11, medan
                  </p>
                </div>
              </div>
            </div>
            </a>
          </div>
          <!-- End of the accordion -->
        </div>
        <!-- End of the day -->
      </div>
      <!-- Carousel nav -->
     
      <div class="gap">
      </div>
    </div>
  </div>
  </div>
</div>
         </div>
      </div>
   </section><!--/.header-section-->

   	<?php include 'footer.php';?>
   	<?php include 'footer_js.php';?>
   
</body>

</html>