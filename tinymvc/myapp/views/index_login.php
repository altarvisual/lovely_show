<?php include 'header.php';?>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1620177954865202', // App ID
      channelUrl : 'http://localhost/facebook-javascript-sdk/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
    
    
	FB.Event.subscribe('auth.authResponseChange', function(response) 
	{
 	 if (response.status === 'connected') 
  	{
  		document.getElementById("message").innerHTML +=  "<br>Connected to Facebook";
  		//SUCCESS
  		
  	}	 
	else if (response.status === 'not_authorized') 
    {
    	document.getElementById("message").innerHTML +=  "<br>Failed to Connect";

		//FAILED
    } else 
    {
    	document.getElementById("message").innerHTML +=  "<br>Logged Out";

    	//UNKNOWN ERROR
    }
	});	
	
    };
    
   	function Login()
	{
	
		FB.login(function(response) {
		   if (response.authResponse) 
		   {
		    	getUserInfo();
  			} else 
  			{
  	    	 console.log('User cancelled login or did not fully authorize.');
   			}
		 },{scope: 'email'});
	
	
	}

  function getUserInfo() {
	    FB.api('/me', function(response) {

	  var str="<b>Name</b> : "+response.name+"<br>";
	  	  str +="<b>Link: </b>"+response.link+"<br>";
	  	  str +="<b>Username:</b> "+response.username+"<br>";
	  	  str +="<b>id: </b>"+response.id+"<br>";
	  	  str +="<b>Email:</b> "+response.email+"<br>";
	  	  str +="<input type='button' value='Get Photo' onclick='getPhoto();'/>";
	  	  str +="<input type='button' value='Logout' onclick='Logout();'/>";
	  	  document.getElementById("status").innerHTML=str;
	  	  	    
    });
    }
	function getPhoto()
	{
	  FB.api('/me/picture?type=normal', function(response) {

		  var str="<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
	  	  document.getElementById("status").innerHTML+=str;
	  	  	    
    });
	
	}
	function Logout()
	{
		FB.logout(function(){document.location.reload();});
	}

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));

</script>
<body class="one-page" class>
	<!--header-->
	<section id="top" class="section slide-section slide-login ">
		<header class="">
			<div class="container">
				<div class="row">
					<div class="col-md-11 col-md-offset-1">
						<div class="top">
							<!--//mobile menu button -->
                  </div>
						<!--/.top-->
					</div>
				</div>
			</div>
		</header>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
				<br/>
				<div class="titlebox">
                  <div class="sub-title">
                     <h2 style="font-size: 24px; color: #ef487d" class="lauren">Klik login untuk melakukan vote</h2>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
					<!--/.titlebox-->
					<div class="btn_socmed">
						<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/login/dologin_fb" onclick=""><img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/login_facebook-01.png"></a> 
						<br/>
						<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/login/dologin_twitter"><img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/login_twitter-01.png"></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/.header-section-->

   	<?php include 'footer_js.php';?>
   
</body>

</html>