<?php include 'header.php';?>
<body class="one-page" >
   <!-- <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div> -->
   <!--header-->
   <section id="top" class="section slide-section slide-home " data-overlay="true">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
         <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
               
            </div><!-- /.portfolio-container -->
            </div>
            <div class="row">
            	<div class="col-md-12">
                  <div class="box-container">
					<?=$message?>
					<br/><button onclick="goBack()">Tutup</button>
               	  </div>
               	</div>
            </div>
         </div>
      </div>
   </section><!--/.header-section-->
   
   <script>
	function goBack() {
		window.close();
	}
	</script>

   	<?php include 'footer.php';?>
   	<?php include 'footer_js.php';?>
   
</body>

</html>