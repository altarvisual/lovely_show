<?php include 'header.php';?>
<body class="one-page" >
   <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div>
   <!--header-->
   <section id="top" class="section slide-section slide-home " data-overlay="true">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
         <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     <h2>About</h2>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
               
            </div><!-- /.portfolio-container -->
            </div>
            <div class="row">
            	<div class="col-md-12">
                  <div class="box-container">
					Lovely Show merupakan acara yang dibuat oleh Mustika Puteri berkerja sama
					dengan JKT48 untuk mengadakan Event School to School, Meet & Greet, dan 
					Concert di 5 kota besar yaitu Medan, Makassar, Bandung, Yogyakarta, dan 
					Surabaya. 
               	  </div>
               	</div>
            </div>
         </div>
      </div>
   </section><!--/.header-section-->

   	<?php include 'footer.php';?>
   	<?php include 'footer_js.php';?>
   
</body>

</html>