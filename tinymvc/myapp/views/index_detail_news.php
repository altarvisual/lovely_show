<?php include 'header.php';?>
<body class="one-page" >
   <!-- <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div> -->
   <!--header-->
   <section id="top" class="section slide-section slide-home ">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
	  <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     <h2>News Title</h2>
					 <h4 class="text-dark">11 April 2015</h4>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
               
            </div><!-- /.portfolio-container -->
            </div>
         <div class="row">
            <div class="col-md-12 col-sm-4  ">
                     <div class="portfolio-img">
                        <img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/bg_lovelymood.jpg">
						
					 </div>
               </div>
            </div>
			<div class="mini_gap">
          </div><div class="mini_gap">
          </div>
			<div class="row">
            	<div class="col-md-12">
                  <div class="box-container">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
               	  </div>
               	</div>
            </div>
         </div>
      </div>
      <?php include 'footer.php';?>
   </section><!--/.header-section-->

   	
   	<?php include 'footer_js.php';?>
   
</body>

</html>