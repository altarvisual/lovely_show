<?php include 'header.php';?>
<body class="one-page">
	<div class="loader">
		<span class="loader1 block-loader"></span> <span
			class="loader2 block-loader"></span> <span
			class="loader3 block-loader"></span>
	</div>
	<!--header-->
	<section id="top" class="section slide-section slide-home "
		data-overlay="true">
		<div class="mobile-event">
			<a href="#" class="logo-img"></a>
		</div>
		<header class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-11 col-md-offset-1">
						<div class="top">
							<div class="btn-menu"></div>
							<!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div>
						<!--/.top-->
					</div>
				</div>
			</div>
		</header>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="titlebox">
						<div class="sub-title">
							<h2>Sign Up</h2>
						</div>
						<!--/.sub-title-->
					</div>
					<!--/.titlebox-->

				</div>
				<!-- /.portfolio-container -->
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box-container form-label">
						<form role="form" action="http://<?php echo $_SERVER['HTTP_HOST'];?>/signup/doregister" method="post"> 
							<div class="form-group">
								<label>Email</label> <input class="form-control" type="email" name="email" required>
							</div>
							<div class="form-group">
								<label>Password</label> <input class="form-control" name="password" type="password" required>
							</div>
							<div class="form-group">
								<label>Ulangi Password</label> <input class="form-control" name="repassword" type="password" required>
							</div>
							<br/>
							<div class="form-group">
								<label>Nama Lengkap</label> <input class="form-control" name="name" required>
							</div>
							<div class="form-group">

								<label>Jenis Kelamin</label>
								<div class="radio">
									<label class="radio-inline"> <input type="radio"
										name="gender" id="optionsRadiosInline1"
										value="Pria" required>Pria
									</label> <label class="radio-inline"> <input type="radio"
										name="gender" id="optionsRadiosInline2"
										value="Wanita" required>Wanita
									</label>
								</div>
							</div>
							<div class="form-group">
								<label>Tempat & Tanggal Lahir</label> 
								<div class="form-inline">
								<input class="form-control" name="birthplace" required> <input class="form-control datepicker" name="birthdate" required>
								</div>
							</div>
							<div class="form-group">
								<label>Telp</label> <input class="form-control" name="telp" type="tel" required>
							</div>
							<div class="form-group">
								<label>Captcha</label> 
								<div class="form-inline">
								<img src="http://<?php echo $_SERVER['HTTP_HOST'];?>/signup/captcha" /> <input class="form-control" required>
								</div>
							</div>

							<button type="submit" class="btn btn-default">Submit Button</button>
							<button type="reset" class="btn btn-default">Reset Button</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
	<!--/.header-section-->

   	<?php include 'footer.php';?>
   	<?php include 'footer_js.php';?>
   	<script type="text/javascript">
   	$('.datepicker').datepicker({
   	    format: 'dd/mm/yyyy',
   	    startDate: '-3d'
   	})
   	</script>
   
</body>

</html>