<?php include 'header.php';?>
<body class="one-page" >
   <!-- <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div> -->
   <!--header-->
   <section id="top menu" class="section slide-section slide-home menu-section">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
         <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     <h2 class="lauren">Winners</h2>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
               
            </div><!-- /.portfolio-container -->
            </div>
            <div class="row">
            	<div class="col-md-12">
               <div class="filter culi-animation" data-portfolio-effect="fadeInDown" data-animation-delay="0" data-animation-offset="85%">
                  <ul class="class-filter">
                  	<li>
                        <a data-filter=".bandung" href="#" class="">
                           <div class="topic text-dark">Bandung</div>
                        </a>
                     </li>
                     <li>
                        <a data-filter=".medan" href="#" class="clear-left active">
                           <div class="topic text-dark">Medan</div>
                        </a>
                     </li>
                     <li>
                        <a data-filter=".makassar" href="#" class="">
                           <div class="topic text-dark">Makassar</div>
                        </a>
                     </li>
                     <li>
                        <a data-filter=".joglosemar" href="#" class="">
                           <div class="topic text-dark">Joglosemar</div>
                        </a>
                     </li>
                     <li>
                        <a data-filter=".surabaya" href="#" class="">
                           <div class="topic text-dark">Surabaya</div>
                        </a>
                     </li>
                     
                  </ul>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="portfolio-container culi-animation" data-portfolio-effect="fadeInDown" data-animation-delay="0" data-animation-offset="75%">
               <div class="col-md-12 col-sm-4 portfolio-items medan">
               <h3>Juara Utama</h3>
                     <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="row  medan">
               <h4>Juara Hiburan</h4>
               <div class="col-md-6 col-sm-4 portfolio-items medan">
               
               <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="col-md-6 col-sm-4 portfolio-items medan">
               	<div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               </div>
               
               <div class="col-md-12 col-sm-4 portfolio-items makassar">
               <h3>Juara Utama</h3>
                     <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="row  makassar">
               <h4>Juara Hiburan</h4>
               <div class="col-md-6 col-sm-4 portfolio-items makassar">
               
               <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="col-md-6 col-sm-4 portfolio-items makassar">
               	<div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               </div>
               
               <div class="col-md-12 col-sm-4 portfolio-items bandung">
               <h3>Juara Utama</h3>
                     <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="row  bandung">
               <h4>Juara Hiburan</h4>
               <div class="col-md-6 col-sm-4 portfolio-items bandung">
               
               <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="col-md-6 col-sm-4 portfolio-items bandung">
               	<div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               </div>
               
               <div class="col-md-12 col-sm-4 portfolio-items joglosemar">
               <h3>Juara Utama</h3>
                     <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="row  joglosemar">
               <h4>Juara Hiburan</h4>
               <div class="col-md-6 col-sm-4 portfolio-items joglosemar">
               
               <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="col-md-6 col-sm-4 portfolio-items joglosemar">
               	<div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               </div>
               
               <div class="col-md-12 col-sm-4 portfolio-items surabaya">
               <h3>Juara Utama</h3>
                     <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="row  surabaya">
               <h4>Juara Hiburan</h4>
               <div class="col-md-6 col-sm-4 portfolio-items surabaya">
               
               <div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               <div class="col-md-6 col-sm-4 portfolio-items surabaya">
               	<div class="portfolio-img">
                        <iframe width="560" height="349" src="http://www.youtube.com/embed/Epgzd1O3Vv4?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					 </div>
               </div>
               </div>
               
            </div><!-- /.portfolio-container -->
            </div>			
      </div>
      <?php include 'footer.php';?>
   </section><!--/.header-section-->

   	
   	<?php include 'footer_js.php';?>
   
</body>

</html>