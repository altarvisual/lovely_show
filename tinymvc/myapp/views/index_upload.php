<?php include 'header.php';?>
<body class="one-page" class>
	<div class="loader">
		<span class="loader1 block-loader"></span> <span
			class="loader2 block-loader"></span> <span
			class="loader3 block-loader"></span>
	</div>
	<!--header-->
	<section id="top" class="section slide-section slide-home " >
		<div class="mobile-event">
			<a href="#" class="logo-img"></a>
		</div>
		<header class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-11 col-md-offset-1">
						<div class="top">
							<div class="btn-menu"></div>
							<!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div>
						<!--/.top-->
					</div>
				</div>
			</div>
		</header>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="titlebox">
						<div class="upload-title text-dark">Isi data dirimu disini, untuk mengikuti kontes video putery
								lovely show</div>
						<div class="upload-warn text-dark">Data yang diisi harus valid dan bisa dipertanggung jawabkan</div>
						<!--/.sub-title-->
					</div>
					<!--/.titlebox-->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box-container form-label">
						<form role="form"
							action="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/doupload"
							method="post">
							<div class="form-group">
								<label>Judul Video</label> <input class="form-control"
									name="title" required>
							</div>
							<div class="form-group">
								<label>Deskripsi</label>
								<textarea class="form-control" rows="3" name="desc"></textarea>
							</div>
							<div class="form-group">
								<label>Nama Group/Sekolah</label> <input class="form-control"
									name="group" required>
							</div>
							<div class="form-group">
								<label>Kota Perform</label> <select class="form-control" name="city" required>
									<option>Medan</option>
									<option>Makassar</option>
									<option>Bandung</option>
									<option>Yogyakarta</option>
									<option>Surabaya</option>
								</select>
							</div>
							<div class="form-group">
								<label>Link Youtube</label> 
								<label class="note-form">Upload video mu di youtube, lalu copy link nya disini ex : https://www.youtube.com/watch?v=17j0ZOL2xfs <br/>Masukkan kode 17j0ZOL2xfs</label> 
								<div class="form-inline">
								<input class="form-control" name="link" size="45%" required>
								<a target="_blank" href="https://www.youtube.com/upload">Utube Link</a>
								</div>
							</div>
							<div class="form-group">
								<label>Captcha</label> 
								<div class="form-inline">
								<img src="http://<?php echo $_SERVER['HTTP_HOST'];?>/signup/captcha" /> <input class="form-control" required>
								</div>
							</div>
							<div align="right"><button type="submit" class="btn btn-default">Submit Button</button></div>
						</form>
					</div>
				</div>
			</div>
			<!-- <div class="row">
            <div class="col-md-12">
               <div class="box-container">
					<form method="post" action="#" id="subscribe-form" data-mailchimp="true" class="form-letter">
						<div id="subscribe-content">
							<div class="input">
							<table id="form">
								<tr>
								<td>Judul Video</td>
								<td></td>
								<td><input type="text" id="subscribe-email" class="form-upload" name="email"> 
								</td>
								</tr>
								<tr>
								<td>Deskripsi</td>
								<td></td>
								<td><textarea></textarea>
								</td>
								</tr>
								<tr>
								<td colspan="2">Nama Group/Sekolah</td>
								<td><input type="text" id="subscribe-email" class="form-upload" name="email"> 
								</td>
								</tr>
								<tr>
								<td>Link Youtube</td>
								<td><img alt="image" src="../template/images/icons/fork.png"></td>
								<td><input type="text" id="subscribe-email" class="form-upload" name="email"> 
								</td>
								</tr>
								<tr>
								<td>Captcha</td>
								<td></td>
								<td><input type="text" id="subscribe-email" class="form-upload" name="email"> 
								</td>
								</tr>
								<tr align="right">
								<td align="right"><button>Submit</button>
								</td>
								</tr>
							</table>
							</div>
						</div>
					</form>
               </div>
            </div>
         </div> -->
		</div>
	</section>
	<!--/.header-section-->

   	<?php include 'footer.php';?>
   	<?php include 'footer_js.php';?>
   
</body>

</html>