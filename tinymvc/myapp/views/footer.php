<!--footer-->
   <footer class="footer">
      <span class="footer-left footer-product"><img alt="image" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/lovelymood_everyday.png"></span>
      <section class="footer-copy">
         <div class="container">
            <div class="row">
               <!-- <div class="col-md-6">
                  <span class="footer-left">Copyright 2015 &copy; Mustika Puteri. All rights are reserved.</span>
               </div>
               <div class="col-md-6">
                 <span class="footer-right"><a href="#" class="facebook">
                           <i class="fa fa-facebook"> Mustika Puteri&nbsp;&nbsp;</i>
                        </a>
                        <a href="#" class="twitter">
                           <i class="fa fa-twitter"> Mustika Puteri</i>
                        </a>
                  </span>
               </div> -->
               <div class="col-md-12" style="bottom: 20px">
               <div>
               	<span style="text-align: center center;">Copyright 2015 &copy; Mustika Puteri. All rights are reserved.</span>
               	<span class="footer-right">
               		<a href="http://facebook.com/mustikaputeriid" target="_blank">
               		<img style="width: 8%;" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/facebook.png"
               		onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/facebook_hover.png'" 
               		onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/facebook.png'" >  </a>
               		<a href="http://twitter.com/mustikaputeriid" target="_blank">
               		<img style="width: 8%;" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/twitter.png"
               		onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/twitter_hover.png'" 
               		onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/twitter.png'">  </a>
               		<a href="https://www.youtube.com/channel/UCqwctNW_RlgwowL87EGkhNQ" target="_blank">
               		<img style="width: 8%;" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/youtube.png"
               		onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/youtube_hover.png'" 
               		onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/youtube.png'">  </a>
               		<a href="http://instagram.com/mustikaputeriid" target="_blank">
               		<img style="width: 8%;" src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/instagram.png"
               		onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/instagram_hover.png'" 
               		onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/instagram.png'">  </a>
              	</span>
                </div>
               </div>
            </div>
         </div>
      </section><!--/.footer-section-->
   </footer>

   <!--go top-->
   <div id='bttop'></div>
