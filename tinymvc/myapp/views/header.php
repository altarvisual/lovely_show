<?php
// Start the session
if(!isset($_SESSION)){
    session_start();
}
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
   <!-- Basic Page Needs -->
   <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
   <title>Mustika Puteri Lovely Show</title>
	<meta name='keywords' content=''>
	<meta name='description' content='Mustika Puteri'>
	<meta name='author' content='altarvisual.com' >

   <!-- Mobile Specific Metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   <!-- Bootstrap  -->
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/bootstrap.css" >
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/schedule.css">
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/color.css">
   
   <!-- Theme Style -->
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/style.css">
   <link href="//dmypbau5frl9g.cloudfront.net/assets/market/pages/preview/index-a753623b16277de3e3abddf9d88cf3f8.css" media="all" rel="stylesheet" type="text/css" />
   <!-- Animation Style -->
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/animate.css">
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/maxcdn_bootstrap.css">
   <link rel='stylesheet' id='style-css'  href='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/video.css' type='text/css' media='all' />
   <!-- Google Fonts -->
   <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
   
   <!--popup stylesheets-->
    <!-- <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,600,700' rel='stylesheet' type='text/css'>
    <link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/page.css" rel="stylesheet"/>
    <link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/portBox.css" rel="stylesheet" />

   <!-- Favicon and touch icons  -->
   <link href="template/icon/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
   <link href="template/icon/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
   <link href="template/icon/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
   <link href="template/icon/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed">
   <link href="icon/favicon.png" rel="shortcut icon">

   <!--[if lt IE 9]>
      <script src="http://localhost/template/javascript/html5shiv.js"></script>
      <script src="http://localhost/template/javascript/respond.min.js"></script>
   <![endif]-->
   
</head>