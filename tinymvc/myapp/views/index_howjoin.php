<?php include 'header.php';?>
<body class="one-page" >
   <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div>
   <!--header-->
   <section id="top" class="section slide-section slide-home ">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
         <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     <h2>How to Join</h2>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
               
            </div><!-- /.portfolio-container -->
            </div>
            <div class="row">
            	<div class="col-md-12">
                  <div class="box-container howjoin">
					Untuk mengikuti video dance contest with JKT 48 caranya mudah :
					<br/><br/>
					<ol>
					<li>Login dulu di <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/login">www.lovelyshow.com</a>, masukan data dirimu dengan benar.</li>
					<li>Baca dulu syarat den ketentuan untuk mengikuti video dance contest ini disini <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/rules">Terms & Condition</a>.</li>
					<li>Masuk ke upload video, isi form yang tesedia lalu copy link video di tempat yang telah kami sediakan.</li>
					<li>Submit video mu, dan akan muncul kata2 selamat anda telah berhasil mengikuti video dance contest with JKT 48.</li>
					<li>Ajak teman-temanmu untuk memvote video terbaikmu, vote terbanyak akan menjadi pemenangnya.</li>
					<li>Pemenang akan mendapatkan kesempatan untuk meet & greet bareng JKT48.</li>
					</ol>
               	  </div>
               	</div>
            </div>
         </div>
      </div>
   </section><!--/.header-section-->

   	<?php include 'footer.php';?>
   	<?php include 'footer_js.php';?>
   
</body>

</html>