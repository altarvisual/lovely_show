<?php
// Start the session
if(!isset($_SESSION)){
    session_start();
}
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
   <!-- Basic Page Needs -->
   <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
   <title>Mustika Puteri Lovely Show</title>
	<meta name='keywords' content=''>
	<meta name='description' content='Mustika Puteri'>
	<meta name='author' content='altarvisual.com' >

   <!-- Mobile Specific Metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   <!-- Bootstrap  -->
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/bootstrap.css" >
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/schedule.css">
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/color.css">
   
   <!-- Theme Style -->
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/style.css">
   <link href="//dmypbau5frl9g.cloudfront.net/assets/market/pages/preview/index-a753623b16277de3e3abddf9d88cf3f8.css" media="all" rel="stylesheet" type="text/css" />
   <!-- Animation Style -->
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/animate.css">
   <link rel="stylesheet" type="text/css" href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/maxcdn_bootstrap.css">
   <link rel='stylesheet' id='style-css'  href='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/video.css' type='text/css' media='all' />
   <!-- Google Fonts -->
   <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
   
   <!--popup stylesheets-->
    <!-- <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,600,700' rel='stylesheet' type='text/css'>
    <link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/page.css" rel="stylesheet"/>
    <link href="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/stylesheets/portBox.css" rel="stylesheet" />

   <!-- Favicon and touch icons  -->
   <link href="template/icon/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
   <link href="template/icon/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
   <link href="template/icon/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
   <link href="template/icon/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed">
   <link href="icon/favicon.png" rel="shortcut icon">
   
<!--    <meta property="og:site_name" content="Your Website Name Here"/> 
   <meta property="og:image" content="http://img.youtube.com/vi/<?=$video['youtube_link']?>/0.jpg" />-->
   
   <meta property="fb:app_id"          content="1234567890" /> 
    <meta property="og:type"            content="article" /> 
    <meta property="og:url"             content="http://newsblog.org/news/136756249803614" /> 
    <meta property="og:title"           content="Introducing our New Site" /> 
    <meta property="og:image"           content="https://fbcdn-dragon-a.akamaihd.net/hphotos-ak-xpa1/t39.2178-6/851565_496755187057665_544240989_n.jpg" /> 
    <meta property="og:description"    content="http://samples.ogp.me/390580850990722" />

   <!--[if lt IE 9]>
      <script src="http://localhost/template/javascript/html5shiv.js"></script>
      <script src="http://localhost/template/javascript/respond.min.js"></script>
   <![endif]-->
   
</head>

<body class="one-page" >
   <!-- <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div> -->
   <!--header-->
   <section id="top" class="section slide-section slide-home ">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
      	<div class="row">
            <div class="col-md-12">
            <ol class="breadcrumb breadcrumb-font ">
			  <li><a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/gallery">Gallery</a></li>
			  <li class="active">Video <?=$video['title']?></li>
			</ol>
            </div>
        </div>
      
         <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     <h2 class="segoui"><?=$video['title']?></h2>
                     <h5 class="segoui">By: <?=$video['group']?></h5>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
               
            </div><!-- /.portfolio-container -->
            </div>
            <div class="row">
            	<div class="col-md-2">
               	</div>
               	<div class="col-md-8">
                  	<div class="videoWrapper">
					    <!-- Copy & Pasted from YouTube -->
					    <iframe width="560" height="349" src="http://www.youtube.com/embed/<?=$video['youtube_link']?>?rel=0&hd=1" frameborder="0" allowfullscreen></iframe>
					</div>
               	</div>
               	<div class="col-md-1">
               		<div class="widget widget-brand">
						<div class="social">
				<div class="fb-share-button" data-href="http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/video?v=<?=$video['video_id']?> " data-layout="button"></div>
				<a style="margin-bottom: 10px;" 
				href="https://www.facebook.com/dialog/feed?
					  app_id=1436356323330822
					  &display=popup&caption=<?=$video['title']?>
					  &link=http://puterilovelyshow.com
					  &redirect_uri=http://puterilovelyshow.com
					  &picture=http://img.youtube.com/vi/<?=$video['youtube_link']?>/0.jpg
					" target="_blank">
					<img src="http://www.simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" /></a><br/>
				<a style="margin-bottom: 10px;" href="http://twitter.com/share?url=http://<?php echo $_SERVER['HTTP_HOST'];?>/dancecontest/video/<?=$video['video_id']?>&text=&hashtags=PuteriLovelyShow" target="_blank"><img src="http://www.simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" /></a>
	           <br/> <a style="margin-bottom: 10px;" href="#" onclick="like()"><img src="http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/vote.png" alt="Vote" /></a>
	                     </div> 
                     </div>
               	</div>
               	<div class="col-md-1">
               	</div>
            </div>
         </div>
      </div>
      <?php include 'footer.php';?>
   </section><!--/.header-section-->
   
   <script type="text/javascript">
		function like() {
			window.open("http://<?php echo $_SERVER['HTTP_HOST'];?>/login/like/<?=$video['video_id']?>", "windowname1", 'width=600, height=450');
		}
   </script>

   	<?php include 'footer_js.php';?>
   
</body>

</html>