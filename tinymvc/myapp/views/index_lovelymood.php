<?php include 'header.php';?>
<body class="one-page" class>
   <!-- <div class="loader">
      <span class="loader1 block-loader"></span>
      <span class="loader2 block-loader"></span>
      <span class="loader3 block-loader"></span>
   </div> -->
   <!--header-->
   <section id="top" class="section slide-section slide-home ">
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
         <div class="row">
            <div class="col-md-12">
            	<div class="titlebox">
                  <div class="sub-title">
                     <h2 class="lauren">Lovely Mood</h2>
                  </div><!--/.sub-title-->
               </div><!--/.titlebox-->
               <div class="" data-portfolio-effect="fadeInDown" data-animation-delay="0" data-animation-offset="75%">
			   <div class="col-md-4 col-sm-4 portfolio-items">
                        <img alt="image" src="template/images/product/puteri-lovelymood-dreamy.png">
                        
               </div>
               <div class="col-md-4 col-sm-4 portfolio-items">
                        <img alt="image" src="template/images/product/puteri-lovelymood-cheerful.png"><br/>
	 					          
               </div>
               
               <div class="col-md-4 col-sm-4  portfolio-items">
                        <img alt="image" src="template/images/product/puteri-lovelymood-inlove.png">

               </div>
            </div><!-- /.portfolio-container -->
            </div>
         </div>
      </div>
      <?php include 'footer.php';?>
   </section><!--/.header-section-->

   	<?php include 'footer_js.php';?>
   
</body>

</html>