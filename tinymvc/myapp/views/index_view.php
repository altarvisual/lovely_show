<?php include 'header.php';?>
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script>
//     $( window ).load(function() {
//     	var portBoxID = 'welcome';
//     	$('#'+portBoxID).display($(this).data());	
//     });

    </script>
<body class="one-page">
   <!--header-->
   <section id="top" class="section slide-section slide-home scroll-texts" >
      <div class="mobile-event"><a href="#" class="logo-img"></a></div>
      <header class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-11 col-md-offset-1">
                  <div class="top">
                     <div class="btn-menu"></div><!--//mobile menu button -->
                     <?php include 'navigation.php';?>
                  </div><!--/.top-->
               </div>
            </div>
         </div> 
      </header>

      <div class="container">
         <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
            	<img alt="image" src="template/images/lovelyshowJKT48_logo.png" class="logo-home">
            </div>
            <div class="col-md-2 ">
            	<div class="home-img">
            	<div class="box-home">
            		<div class="text-white video-text">Video Dance Contest</div>
            		<a href="#" data-display="howtojoin" data-closeBGclick="true">
            		<img alt="image" src="template/images/button/how_to_join.png"
            		onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/how_to_join_hover.png'" 
               		onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/how_to_join.png'"></a>
            		<a href="#" data-display="city" data-closeBGclick="true">
            		<img alt="image" src="template/images/button/upload_video.png"
            		onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/upload_video_hover.png'" 
               		onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/upload_video.png'"></a>
            	</div>
            	<div class="box-roadshow">
            	 <a href="#" data-display="roadshow" data-closeBGclick="true">
	            <img alt="image" src="template/images/button/roadshow_button.png" 
	            onmouseover="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/roadshow_button_hover.png'" 
               	onmouseout="this.src='http://<?php echo $_SERVER['HTTP_HOST'];?>/template/images/button/roadshow_button.png'"
	            class="roadshow-btn"></a>
            	</div>
            	
            	</div>
	           
            </div>
         </div>
      </div>
<?php include 'footer.php';?>
   </section><!--/.header-section-->
   
  

   
   <?php include 'footer_js.php';?>
   
</body>

</html>