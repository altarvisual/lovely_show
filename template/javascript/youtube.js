/*
Copyright 2013 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

(function() {
  var GOOGLE_PLUS_SCRIPT_URL = 'https://apis.google.com/js/client:plusone.js';
  var CHANNELS_SERVICE_URL = 'https://www.googleapis.com/youtube/v3/channels';
  var USER_SERVICE_URL = 'https://www.googleapis.com/oauth2/v1/userinfo';
  var VIDEOS_UPLOAD_SERVICE_URL = 'https://www.googleapis.com/upload/youtube/v3/videos?uploadType=resumable&part=snippet';
  var VIDEOS_SERVICE_URL = 'https://www.googleapis.com/youtube/v3/videos';
  var INITIAL_STATUS_POLLING_INTERVAL_MS = 15 * 1000;

  var accessToken;
  var email;

  window.oauth2Callback = function(authResult) {
    if (authResult['access_token']) {
      accessToken = authResult['access_token'];
      
      $.ajax({
          url: USER_SERVICE_URL,
          method: 'GET',
          headers: {
            Authorization: 'Bearer ' + accessToken
          },
          data: {
            part: 'snippet',
            mine: true
          }
        }).done(function(response) {
        	email = response.email;
        	$('#email').val(email);
       		$('#name').val(response.name);
        });
      

      $.ajax({
        url: CHANNELS_SERVICE_URL,
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + accessToken
        },
        data: {
          part: 'snippet',
          mine: true
        }
      }).done(function(response) {
        $('#channel-name').text(response.items[0].snippet.title);
        $('#channel-thumbnail').attr('src', response.items[0].snippet.thumbnails.default.url);
        
        var portBoxID = 'upload';

        $('.pre-sign-in').hide();
        $('.post-sign-in').show();
        
			var top, left;
			var portBox = $('#'+portBoxID), 
						scrollBar;
			
			if(portBox.outerHeight() + 25 > $(window).height()){
				 
				 var maxH = $(window).height() - 80;
				 
				portBox.css({'height' :maxH + 'px'});	 
			}
			
			portBox.css({
				top:0, 
				left:0
			});
			
			top = Math.max($(window).height() - portBox.outerHeight(), 0) / 2;
			left = Math.max($(window).width() - portBox.outerWidth(), 0) / 2;
			
			//Set top and left values for portBox to center it 
			
			portBox.css({
				top:top + $(window).scrollTop(), 
				left:left + $(window).scrollLeft()
			});
			
			if(portBox.outerHeight() >= $(window).height() && scrollBar == false){
				portBox.css({'top' : 20 + $(window).scrollTop()});
			}
			
			if (typeof $.fn.slimScroll == 'function'){
				scrollBar = true;
			}else{
				scrollBar = false;	
			};
			
			if (portBox.has(".scrollBar").length == 0 && scrollBar == true){
				
				//Wrap Everything in the portBox div in a new div with class of scrollBar
				//Add 20 padding on right and set portBox padding to 10 to offset inclusion of scroll bar
				portBox.wrapInner('<div class="scrollBar" style="padding-right:20px;" />'),
				portBox.css({'padding-right' : 10});
				
				//Run the slimScroll function
				$(function(){
					$('.scrollBar').slimScroll({
						height: '100%'
					});
				});	
				
				//Add Close button anchor tag last so it's not traped in slimScroll div
				portBox.append('<a class="close-portBox">&#215;</a>');		
			}
//			
//			//If no Close button exists add one
//			if (portBox.has(".close-portBox").length == 0 && scrollBar == false){
//				
//				portBox.append('<a class="close-portBox">&#215;</a>');
//			}
//			
//
//			var top, left;
//			
//				//If window width is greater than or equal to 1025px 
//				//OR box height	is greater than window height 
//				//AND scrollBar is TRUE set height to 90%
//				 if(portBox.outerHeight() + 25 > $(window).height() && scrollBar === true && mobileIE === false){
//					 
//					 var maxH = $(window).height() - 80;
//					 
//					portBox.css({'height' :maxH + 'px'});	 
//				}
//				
//				//Set top an left to 0 <-- seems unnecessary but fixed window offsetting problems when box first appears
//				portBox.css({
//					top:0, 
//					left:0
//				});
//			
//				//Get the windows height minus the portBox height divide it in half
//				//Get the windows width minus the portBox width divide it in half
//				
//				top = Math.max($(window).height() - portBox.outerHeight(), 0) / 2;
//				left = Math.max($(window).width() - portBox.outerWidth(), 0) / 2;
//				
//				//Set top and left values for portBox to center it 
//				
//				portBox.css({
//					top:top + $(window).scrollTop(), 
//					left:left + $(window).scrollLeft()
//				});
//				// If window width greater than or equal to portBox width move close icon inside box 
//				//and leave 5px margin on either side of box
//				
//				if(portBox.innerWidth() + 10 >= $(window).width()){
//					portBox.css({'margin-right' : 5 + 'px'}),
//					$('.close-portBox').css({'top': 3, 'right': 3});
//					
//				// Else no margin and position close icon upper right hanging off corner		
//				}else{
//					portBox.css({'margin-right' : 0 + 'px'}),
//					$('.close-portBox').css({'top': -6, 'right': -7});	
//				}
//				
//				//If portBox height is greater than or equal to window height add top of 20 
//				if(portBox.outerHeight() >= $(window).height() && scrollBar == false){
//					portBox.css({'top' : 20 + $(window).scrollTop()});
//				}

			
			

        
//        $('#'+portBoxID).css({'display' : 'none'});
//        $('#'+portBoxID).display($(this).data());	
      });
    }
  };

  function initiateUpload(e) {
    e.preventDefault();

    var file = $('#file').get(0).files[0];
    if (file) {
      $('#submit').attr('disabled', true);

      var metadata = {
        snippet: {
          title: $('#title').val(),
          description: $('#description').val(),
          categoryId: 22
        }
      };

      $.ajax({
        url: VIDEOS_UPLOAD_SERVICE_URL,
        method: 'POST',
        contentType: 'application/json',
        headers: {
          Authorization: 'Bearer ' + accessToken,
          'x-upload-content-length': file.size,
          'x-upload-content-type': file.type
        },
        data: JSON.stringify(metadata)
      }).done(function(data, textStatus, jqXHR) {
        resumableUpload({
          url: jqXHR.getResponseHeader('Location'),
          file: file,
          start: 0
        });
      });
      
//      var dataString = 'title='+ $('#title').val() + '&desc='+ $('#description').val() + '&group='+ $('#group').val();
//      $.ajax({
//    	  type: "POST",
//    	  url: "http://localhost/lovely/dancecontest/doupload",
//    	  data: dataString,
//    	  cache: false,
//    	  success: function(result){
//    	  alert(result);
//    	  }
//    	  });
    }
  }

  function resumableUpload(options) {
    var ajax = $.ajax({
      url: options.url,
      method: 'PUT',
      contentType: options.file.type,
      headers: {
        'Content-Range': 'bytes ' + options.start + '-' + (options.file.size - 1) + '/' + options.file.size
      },
      xhr: function() {
        // Thanks to http://stackoverflow.com/a/8758614/385997
        var xhr = $.ajaxSettings.xhr();

        if (xhr.upload) {
          xhr.upload.addEventListener(
            'progress',
            function(e) {
              if(e.lengthComputable) {
                var bytesTransferred = e.loaded;
                var totalBytes = e.total;
                var percentage = Math.round(100 * bytesTransferred / totalBytes);

                $('#upload-progress').attr({
                  value: bytesTransferred,
                  max: totalBytes
                });

                $('#percent-transferred').text(percentage);
                $('#bytes-transferred').text(bytesTransferred);
                $('#total-bytes').text(totalBytes);

                $('.during-upload').show();
              }
            },
            false
          );
        }

        return xhr;
      },
      processData: false,
      data: options.file
    });

    ajax.done(function(response) {
      var videoId = response.id;
      
      var dataString1 = 'email='+ $('#email').val() + '&name='+ $('#name').val() + '&address='+ $('#address').val() + '&telp='+ $('#telp').val();
 	 	$.ajax({
    	  type: "POST",
    	  url: "http://localhost/lovely/signup/doregister_google",
    	  data: dataString1,
    	  cache: false,
    	  success: function(result){
    		
    	  }
    	  });
      
      var dataString = 'title='+ $('#title').val() + '&desc='+ $('#description').val() + '&group='+ $('#group').val() + '&city='+ $('#city-perform').text() + '&email='+ $('#email').val() + '&link=' + videoId;
      $.ajax({
    	  type: "POST",
    	  url: "http://localhost/lovely/dancecontest/doupload",
    	  data: dataString,
    	  cache: false,
    	  success: function(result){
//    		  alert(result);
    	  }
    	  });
      
//      $('#video-id').text(videoId);
      $('.post-upload').show();
      checkVideoStatus(videoId, INITIAL_STATUS_POLLING_INTERVAL_MS);
    });

    ajax.fail(function() {
      $('#submit').click(function() {
        alert('Not yet implemented!');
      });
      $('#submit').val('Resume Upload');
      $('#submit').attr('disabled', false);
    });
  }

  function checkVideoStatus(videoId, waitForNextPoll) {
    $.ajax({
      url: VIDEOS_SERVICE_URL,
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + accessToken
      },
      data: {
        part: 'status,processingDetails,player',
        id: videoId
      }
    }).done(function(response) {
      var processingStatus = response.items[0].processingDetails.processingStatus;
      var uploadStatus = response.items[0].status.uploadStatus;

//      $('#post-upload-status').append('<li>Processing status: ' + processingStatus + ', upload status: ' + uploadStatus + '</li>');

      if (processingStatus == 'processing') {
        setTimeout(function() {
          checkVideoStatus(videoId, waitForNextPoll * 2);
        }, waitForNextPoll);
      } else {
        if (uploadStatus == 'processed') {
          $('#player').append(response.items[0].player.embedHtml);
        }

        $('#post-upload-status').append('<li>Final status.</li>');
        alert("Upload berhasil, terima kasih telah berpartisipasi event ini. Ajak teman-temanmu untuk vote video kamu!");
        window.location = 'http://localhost/lovely/dancecontest/gallery';
      }
    });
  }

  $(function() {
    $.getScript(GOOGLE_PLUS_SCRIPT_URL);
    
    var fileInput = $('#file');
    var maxSize = fileInput.data('max-size');
    $('#upload-form').submit(function(e){
        if(fileInput.get(0).files.length){
            var fileSize = fileInput.get(0).files[0].size; // in bytes
            if(fileSize>maxSize){
                alert('Ukuran file harus dibawah 100 MB');
                return false;
            } else {
            	initiateUpload(e);
            }
        }

    });
    
    //$('#upload-form').submit(initiateUpload);
  });
})();